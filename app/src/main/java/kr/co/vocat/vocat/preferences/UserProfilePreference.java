package kr.co.vocat.vocat.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.models.UserProfile;

/**
 * Created by Shark on 2016-01-25.
 */
public class UserProfilePreference {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private static final String PREFER_NAME = Utils.PREF_NAME;
    private static final String KEY_SEX = "sex";
    private static final String KEY_DATE_OF_BIRTH = "date_of_birth";
    private static final String KEY_LANGUAGE_FOR = "language_for";
    private static final String KEY_LANGUAGE_BY = "language_by";

    public UserProfilePreference(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
    }

    public UserProfile getUserProfile() {
        String sex = pref.getString(KEY_SEX, null);
        String date_of_birth = pref.getString(KEY_DATE_OF_BIRTH, null);
        String language_for = pref.getString(KEY_LANGUAGE_FOR, null);
        String language_by = pref.getString(KEY_LANGUAGE_BY, null);
        return new UserProfile(sex, date_of_birth, language_for, language_by);
    }

    public void setUserProfile(UserProfile userProfile) {
        editor = pref.edit();
        editor.putString(KEY_DATE_OF_BIRTH, userProfile.getDateOfBirth());
        editor.putString(KEY_SEX, userProfile.getSex());
        editor.putString(KEY_LANGUAGE_FOR, userProfile.getLanguageFor());
        editor.putString(KEY_LANGUAGE_BY, userProfile.getLanguageBy());
        editor.commit();
    }
    public void deleteUserProfile() {
        editor = pref.edit();
        editor.remove(KEY_SEX);
        editor.remove(KEY_DATE_OF_BIRTH);
        editor.remove(KEY_LANGUAGE_FOR);
        editor.remove(KEY_LANGUAGE_BY);
        editor.commit();
    }

}
