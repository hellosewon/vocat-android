package kr.co.vocat.vocat.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Shark on 2016-01-25.
 */
public class User {

    private int id;
    private String email;
    private String name;

    public User(JSONObject jObject) {
        try{
            this.id = jObject.getInt("id");
            this.email = jObject.getString("email");
            this.name = jObject.getString("name");
        } catch (JSONException e) {
            Log.e("Shark", "User model JSONException!");
        }
    }

    public User(int id, String email, String name) {
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }

    public String getName() {
        return this.name;
    }

}
