package kr.co.vocat.vocat;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Shark on 2016-01-22.
 *
 */
public class Utils {

    //public static final String SERVER_URL = "http://110.76.100.84/";  // KAIST
    //public static final String SERVER_URL = "http://192.168.25.38/";  // Seoul Home
    //public static final String SERVER_URL = "http://192.168.173.1/";  // Custom Wifi
    public static final String SERVER_URL = "http://www.vocat.co.kr/";  // DNS
    public static final String PREF_NAME = "kr.co.vocat.vocat";
    public static final String DATABASE_NAME = "vocat.db";
    public static final int DATABASE_VERSION = 1;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getAPIURL(String url) {
        return SERVER_URL + url;
    }

    public static boolean isEmailValid(String email) {
        return email.contains("@") && email.length() <= 255 && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid(String password) {
        return password.length() > 7 && password.length() <= 128;
    }

    public static boolean isNameValid(String name) {
        //TODO: Replace this with your own logic
        return name.length() > 0 && name.length() <= 70;
    }

    public static SimpleDateFormat _dateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf;
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(new Date());
    }

}
