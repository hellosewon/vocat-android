package kr.co.vocat.vocat.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.activities.StackStudyActivity;
import kr.co.vocat.vocat.adapters.VocabDetailRVAdapter;
import kr.co.vocat.vocat.data.StudyVocabManager;
import kr.co.vocat.vocat.models.StudyVocab;

public class VocabSetFragment extends Fragment {

    private int SET_SIZE = 10;
    private int mPage;
    private Boolean mIsSplit;
    private Integer mStackId;
    private int mStackSize;
    private List<StudyVocab> mSetVocabs;
    private Context mContext;
    private VocabDetailRVAdapter mVocabDetailRVAdapter;
    private RecyclerView rvVocabs;

    private OnVocabSetFragmentInteractionListener mListener = new OnVocabSetFragmentInteractionListener() {
        @Override
        public void onBtnMixClick() {
            Collections.shuffle(mSetVocabs);
            mVocabDetailRVAdapter.refreshItems(mSetVocabs);
            rvVocabs.smoothScrollToPosition(0);
        }
        @Override
        public void onBtnPrevSetClick() {
            ((StackStudyActivity) getActivity()).showPrevSet();
        }
        @Override
        public void onBtnNextSetClick() {
            ((StackStudyActivity) getActivity()).showNextSet();
        }
        @Override
        public void onBtnPronunciationClick(String vocabulary) {
            ((StackStudyActivity) getActivity()).speakOut(vocabulary);
        }
    };

    public static VocabSetFragment newInstance(int page, Boolean isSplit, Integer stackId) {
        Bundle args = new Bundle();
        args.putInt("PAGE", page);
        args.putBoolean("IS_SPLIT", isSplit);
        args.putInt("STACK_ID", stackId);

        VocabSetFragment fragment = new VocabSetFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt("PAGE");
        mIsSplit = getArguments().getBoolean("IS_SPLIT");
        mStackId = getArguments().getInt("STACK_ID");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vocab_set, container, false);
        mContext = view.getContext();

        mStackSize = getStackSize();
        mSetVocabs = getVocabs(mPage-1);

        rvVocabs = (RecyclerView) view.findViewById(R.id.rv_vocabs);
        rvVocabs.setLayoutManager(new LinearLayoutManager(mContext, OrientationHelper.VERTICAL, false));

        mVocabDetailRVAdapter = new VocabDetailRVAdapter(mSetVocabs, mPage, mStackSize, mListener, mContext);
        rvVocabs.setAdapter(mVocabDetailRVAdapter);

        return view;
    }

    private int getStackSize() {
        StudyVocabManager vocabManager = new StudyVocabManager(mContext);
        return vocabManager.getStackStudyVocabs(mStackId).size();
    }

    private List<StudyVocab> getVocabs(int position) {
        StudyVocabManager vocabManager = new StudyVocabManager(mContext);
        List<StudyVocab> stackVocabs = vocabManager.getStackStudyVocabs(mStackId);
        List<StudyVocab> setVocabs;
        if (mIsSplit) {
            int start = position*SET_SIZE;
            int end = SET_SIZE*(position+1);
            if (end <= stackVocabs.size()) {
                setVocabs = stackVocabs.subList(start, end);
            } else {
                setVocabs = stackVocabs.subList(start, stackVocabs.size());
            }
        } else setVocabs = stackVocabs;
        return setVocabs;
    }

    public interface OnVocabSetFragmentInteractionListener {
        void onBtnMixClick();
        void onBtnPrevSetClick();
        void onBtnNextSetClick();
        void onBtnPronunciationClick(String vocabulary);
    }

}