package kr.co.vocat.vocat.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.Utils;


/**
 * A login screen that offers login via email/password.
 */
public class PasswordResetActivity extends AppCompatActivity {
    // Global variable
    private Context mContext;
    private String mEmail;

    // UI references.
    private CoordinatorLayout mCoordinatorLayout;
    private EditText mEmailView;
    private View mProgressView;
    private View mPasswordResetFormView;
    private Button mPasswordResetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);
        // Set up the registration form.
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        mEmailView = (EditText) findViewById(R.id.email);

        mEmailView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptPasswordReset();
                    return true;
                }
                return false;
            }
        });

        mPasswordResetButton = (Button) findViewById(R.id.email_pw_reset_button);
        mPasswordResetButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPasswordReset();
            }
        });

        mPasswordResetFormView = findViewById(R.id.pw_reset_form);
        mProgressView = findViewById(R.id.pw_reset_progress);

        mContext = this;
        retrieveIntent();
        mEmailView.requestFocus();
    }

    private void retrieveIntent() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String email = extras.getString("EMAIL");
            mEmailView.setText(email);
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptPasswordReset() {
        // Reset errors.
        mEmailView.setError(null);

        // Store values at the time of the registration attempt.
        String email = mEmailView.getText().toString();
        mEmail = email;

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Utils.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            if (!Utils.isNetworkAvailable(this)) {
                Snackbar.make(mCoordinatorLayout,
                        R.string.snkbar_no_internet, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.snkbar_actn_retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                attemptPasswordReset();
                            }
                        }).show();
            } else {
                mPasswordResetButton.setEnabled(false);
                showProgress(true);
                resetPassword(email);
            }
        }
    }

    private void resetPassword(String email) {
        String url = Utils.getAPIURL("accounts/password_reset/");
        HashMap<String, String> myParams = new HashMap<>();
        myParams.put("email", email);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST,
                url,
                new JSONObject(myParams),
                reqSuccessListener(),
                reqErrorListener()
        );
        Volley.newRequestQueue(mContext).add(myReq);
    }

    private Response.Listener<JSONObject> reqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                postResponseAction();
                Toast.makeText(mContext,
                        R.string.toast_pw_reset_confirm,
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.putExtra("EMAIL", mEmail);
                intent.putExtra("PASSWORD", "");
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                kill_activity();
            }
        };
    }

    private Response.ErrorListener reqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                postResponseAction();
                if (error.networkResponse == null) {
                    Snackbar.make(mCoordinatorLayout,
                            R.string.snkbar_server_no_response, Snackbar.LENGTH_SHORT).show();
                } else {
                    switch (error.networkResponse.statusCode) {
                        case 400:  // Bad Request. Form mal-formed
                            Snackbar.make(mCoordinatorLayout,
                                    new String(error.networkResponse.data), Snackbar.LENGTH_SHORT).show();
                            break;
                        case 500:  // Internal Server Error.
                            Snackbar.make(mCoordinatorLayout,
                                    R.string.snkbar_server_error, Snackbar.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        };
    }


    private void postResponseAction() {
        showProgress(false);
        mPasswordResetButton.setEnabled(true);
        mEmailView.requestFocus();
    }
    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mPasswordResetFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mPasswordResetFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mPasswordResetFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mPasswordResetFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void kill_activity() {
        finish();
    }

}

