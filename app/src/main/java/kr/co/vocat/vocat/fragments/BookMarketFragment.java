package kr.co.vocat.vocat.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import kr.co.vocat.toolbox.GsonRequest;
import kr.co.vocat.toolbox.MyLinearLayoutManager;
import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.activities.BookContentActivity;
import kr.co.vocat.vocat.activities.BookInfoActivity;
import kr.co.vocat.vocat.adapters.BookMarketRVAdapter;
import kr.co.vocat.vocat.authenticator.VocatUserManager;
import kr.co.vocat.vocat.data.VocatDBManager;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.models.Book;

/**
 * A fragment representing a list of Items.
 * <p />
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class BookMarketFragment extends Fragment {

    private static final String TAG = "BookMarketFragment";

    private Book mSelectedBook;
    private BookMarketRVAdapter mBookMarketRVAdapter;

    private Context mContext;
    private VocatUserManager mUserManager;
    private CoordinatorLayout mCoordinatorLayout;
    private RecyclerView rvBookMarket;
    private LinearLayout mNoInternetLayout;
    private View mProgressView;

    private OnListFragmentInteractionListener mListener = new OnListFragmentInteractionListener() {
        @Override
        public void onListClick(Book book) {
            mSelectedBook = book;
            seeBookInfo();
        }
        @Override
        public void onListLongClick(Book book) {
            mSelectedBook = book;
            rvBookMarket.showContextMenu();
        }
        @Override
        public void onSecondBtnClick(Book book) {
            mSelectedBook = book;
            downloadBook();
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BookMarketFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.nav_bookmarket));

        View view = inflater.inflate(R.layout.fragment_book_market, container, false);
        mContext = view.getContext();

        mUserManager = new VocatUserManager(mContext);

        rvBookMarket = (RecyclerView) view.findViewById(R.id.rv_book_market);
        rvBookMarket.setLayoutManager(new LinearLayoutManager(mContext, OrientationHelper.VERTICAL, false));

        mCoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayout);
        mNoInternetLayout = (LinearLayout) view.findViewById(R.id.empty_no_internet);
        mProgressView = view.findViewById(R.id.book_market_progress);

        mBookMarketRVAdapter = new BookMarketRVAdapter(new ArrayList<Book>(), mListener, mContext);
        rvBookMarket.setAdapter(mBookMarketRVAdapter);
        registerForContextMenu(rvBookMarket);
        populateBookMarket();
        return view;
    }

    private void populateBookMarket() {
        if (!Utils.isNetworkAvailable(mContext)) {
            mNoInternetLayout.setVisibility(View.VISIBLE);
            rvBookMarket.setVisibility(View.GONE);
            Snackbar.make(mCoordinatorLayout,
                    R.string.snkbar_no_internet, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.snkbar_actn_retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            populateBookMarket();
                        }
                    }).show();
        } else {
            if(isAdded()) { showProgress(true); }
            mNoInternetLayout.setVisibility(View.GONE);
            String url = Utils.getAPIURL("book-market/");
            String token = mUserManager.getToken();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Token " + token);
            GsonRequest<Book[]> myReq = new GsonRequest<>(url,
                    Book[].class,
                    headers,
                    bookReqSuccessListener(),
                    bookReqErrorListener()
            );
            Volley.newRequestQueue(mContext).add(myReq);
        }

    }

    private Response.Listener<Book[]> bookReqSuccessListener() {
        return new Response.Listener<Book[]>() {
            @Override
            public void onResponse(Book[] books) {
                mBookMarketRVAdapter.refreshItems(Arrays.asList(books));
                showProgress(false);
            }
        };
    }
    private Response.ErrorListener bookReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
                showProgress(false);
            }
        };
    }

    private void seeBookInfo() {
        Intent intent = new Intent(getActivity(), BookInfoActivity.class);
        intent.putExtra("BOOK_ID", mSelectedBook.getId());
        intent.putExtra("IS_MARKET", true);
        startActivity(intent);
    }

    private void openBook() {
        Intent intent = new Intent(getActivity(), BookContentActivity.class);
        intent.putExtra("BOOK_ID", mSelectedBook.getId());
        startActivity(intent);
    }

    private void downloadBook() {
        showProgress(true);
        Snackbar.make(mCoordinatorLayout, "Downloading...", Snackbar.LENGTH_INDEFINITE).show();
        VocatDBManager dbManager = new VocatDBManager(getActivity());
        dbManager.downloadBook(mSelectedBook.getId(), new VocatDBManager.VolleyCallback() {
            @Override
            public void onResult(boolean success) {
                showProgress(false);
                String msg;
                if (success) msg = "Book downloaded.";
                else msg = "Download failed.";
                Snackbar.make(mCoordinatorLayout, msg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (mSelectedBook == null) {
            return super.onContextItemSelected(item);
        }
        switch (item.getItemId()) {
            case R.id.ctx_menu_book_info:
                seeBookInfo();
                break;
//            case R.id.ctx_menu_oepn_book:
//                openBook();
//                break;
            case R.id.ctx_menu_download_book:
                downloadBook();
                break;
        }
        return super.onContextItemSelected(item);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderIcon(R.drawable.ic_book);
        menu.setHeaderTitle(mSelectedBook.getTitle());
        menu.add(0, R.id.ctx_menu_book_info, 0, R.string.menu_book_info);
        //menu.add(0, R.id.ctx_menu_oepn_book, 1, R.string.menu_open_book);
        menu.add(0, R.id.ctx_menu_download_book, 2, R.string.menu_download_book);
        if (BookORM.findBookById(mContext, mSelectedBook.getId()) != null)
            menu.findItem(R.id.ctx_menu_download_book).setEnabled(false);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if(isAdded()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                rvBookMarket.setVisibility(show ? View.GONE : View.VISIBLE);
                rvBookMarket.animate().setDuration(shortAnimTime).alpha(
                        show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        rvBookMarket.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });

                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mProgressView.animate().setDuration(shortAnimTime).alpha(
                        show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
            } else {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                rvBookMarket.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        }

    }

    public interface OnListFragmentInteractionListener {
        void onListClick(Book book);
        void onListLongClick(Book book);
        void onSecondBtnClick(Book book);
    }
}
