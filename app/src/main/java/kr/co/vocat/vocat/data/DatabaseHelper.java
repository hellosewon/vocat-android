package kr.co.vocat.vocat.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.data.orm.SectionORM;
import kr.co.vocat.vocat.data.orm.StackORM;
import kr.co.vocat.vocat.data.orm.StudyORM;
import kr.co.vocat.vocat.data.orm.StudyVocabORM;
import kr.co.vocat.vocat.data.orm.VocabularyORM;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    public DatabaseHelper(Context context) {
        super(context, Utils.DATABASE_NAME, null, Utils.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(BookORM.DROP_TABLE);
            db.execSQL(SectionORM.DROP_TABLE);
            db.execSQL(VocabularyORM.DROP_TABLE);
            db.execSQL(StudyORM.DROP_TABLE);
            db.execSQL(StackORM.DROP_TABLE);
            db.execSQL(StudyVocabORM.DROP_TABLE);
        } catch (Exception e) {
            Log.e(TAG, "Failed to drop SQL Table.");
        }

        try {
            db.execSQL(BookORM.CREATE_TABLE);
            db.execSQL(SectionORM.CREATE_TABLE);
            db.execSQL(VocabularyORM.CREATE_TABLE);
            db.execSQL(StudyORM.CREATE_TABLE);
            db.execSQL(StackORM.CREATE_TABLE);
            db.execSQL(StudyVocabORM.CREATE_TABLE);
        } catch (Exception e) {
            Log.e(TAG, "Failed to create SQL Table.");
        }
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > 1) {
            onCreate(db);
        }
    }
}
