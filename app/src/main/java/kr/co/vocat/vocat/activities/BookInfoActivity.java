package kr.co.vocat.vocat.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.data.VocatDBManager;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.data.orm.StudyORM;
import kr.co.vocat.vocat.models.Book;

public class BookInfoActivity extends AppCompatActivity {

    private static String TAG = "BookInfoActivity";
    private Book mBook;
    private Boolean mIsMarket;

    private View mProgressView;
    private ScrollView mBookInfoLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mProgressView = findViewById(R.id.book_info_progress);
        mBookInfoLayout = (ScrollView) findViewById(R.id.book_info_layout);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btn_open_book);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String bookId = extras.getString("BOOK_ID");
            mIsMarket = extras.getBoolean("IS_MARKET");
            if (mIsMarket) {
                fab.setVisibility(View.GONE);
                showProgress(true);
                new VocatDBManager(this).getBookOnline(bookId, new VocatDBManager.VolleyBookCallback() {
                    @Override
                    public void onBookResult(Book book) {
                        showProgress(false);
                        populateBookInfo(book);
                    }
                });
            } else {
                populateBookInfo(BookORM.findBookById(this, bookId));

                final Intent intent = new Intent(this, BookContentActivity.class);
                intent.putExtra("BOOK_ID", bookId);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!mIsMarket) startActivity(intent);
                    }
                });
            }
        }
    }

    private void populateBookInfo(Book book) {
        mBook = book;
        //invalidateOptionsMenu();
        long vocabCount;
        if (mIsMarket) {
            vocabCount = book.getVocabCount();
        } else {
            vocabCount = BookORM.countVocabulary(this, book.getId());
        }
        TextView littleInfo = (TextView) findViewById(R.id.little_info);
        ImageButton btnSecondary = (ImageButton) findViewById(R.id.btn_secondary);
        TextView bookTitle = (TextView) findViewById(R.id.book_title);
        TextView bookDescription = (TextView) findViewById(R.id.book_description);
        FrameLayout frameForPublic = (FrameLayout) findViewById(R.id.frame_for_public);
        frameForPublic.setVisibility(View.GONE);  // TODO: do comments, rating stuff

        btnSecondary.setImageResource(R.drawable.ic_file_download);
        littleInfo.setText(getString(R.string.book_little_info, "Public", vocabCount));
        bookDescription.setLines(6);
        if (BookORM.findBookById(this, mBook.getId()) != null) {
            btnSecondary.setBackground(ContextCompat.getDrawable(this, R.drawable.round_button_darkbg));
            btnSecondary.setColorFilter(ContextCompat.getColor(this, R.color.text_disabled_lightbg), android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
            btnSecondary.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadBook();
                }
            });
        }

        bookTitle.setText(mBook.getTitle());
        if (!mBook.getDescription().equals("")) { bookDescription.setText(mBook.getDescription()); }
    }

    private void downloadBook() {
        showProgress(true);
        Snackbar.make(mBookInfoLayout, "Downloading...", Snackbar.LENGTH_INDEFINITE).show();
        VocatDBManager dbManager = new VocatDBManager(this);
        dbManager.downloadBook(mBook.getId(), new VocatDBManager.VolleyCallback() {
            @Override
            public void onResult(boolean success) {
                showProgress(false);
                String msg;
                if (success) msg = "Book downloaded.";
                else msg = "Download failed.";
                Snackbar.make(mBookInfoLayout, msg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!mIsMarket) populateBookInfo(BookORM.findBookById(this, mBook.getId()));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_edit);
        if (mBook!=null && mBook.getIsPublic()) item.setEnabled(false);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (!mIsMarket) getMenuInflater().inflate(R.menu.activity_book_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_edit) {
            Intent intent = new Intent(this, CreateBookActivity.class);
            intent.putExtra("BOOK_ID", mBook.getId());
            startActivity(intent);
            return true;
        } else if (id == R.id.action_delete) {
            Log.d(TAG, "Delete book !");
            final Context mContext = this;
            new AlertDialog.Builder(this)
                    .setTitle(mBook.getTitle())
                    .setMessage("All related sections and vocabularies will be deleted. Delete this book? ")
                    .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            if (new VocatDBManager(mContext).deleteBook(mBook.getId())) {
                                finish();
                            } else {
                                Toast.makeText(mContext, "Deletion failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mBookInfoLayout.setVisibility(show ? View.GONE : View.VISIBLE);
            mBookInfoLayout.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mBookInfoLayout.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mBookInfoLayout.setVisibility(show ? View.GONE : View.VISIBLE);
        }

    }
}
