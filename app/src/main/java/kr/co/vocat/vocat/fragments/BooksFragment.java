package kr.co.vocat.vocat.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import kr.co.vocat.toolbox.MyLinearLayoutManager;
import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.activities.BookContentActivity;
import kr.co.vocat.vocat.activities.BookInfoActivity;
import kr.co.vocat.vocat.activities.CreateBookActivity;
import kr.co.vocat.vocat.activities.CreateStudyActivity;
import kr.co.vocat.vocat.adapters.BooksRVAdapter;
import kr.co.vocat.vocat.data.VocatDBManager;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.models.Book;

/**
 * A fragment representing a list of Items.
 * <p />
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class BooksFragment extends Fragment {

    private static final String TAG = "BooksFragment";

    private Context mContext;
    private RecyclerView rvPrivateBook;
    private Book mSelectedBook;
    private BooksRVAdapter mBooksRVAdapter;
    private OnListFragmentInteractionListener mListener  = new OnListFragmentInteractionListener() {
        @Override
        public void onListClick(Book book) {
            mSelectedBook = book;
            openBook();
        }
        @Override
        public void onListLongClick(Book book) {
            mSelectedBook = book;
            rvPrivateBook.showContextMenu();
        }
        @Override
        public void onSecondBtnClick(Book book) {
            mSelectedBook = book;
            studyBook();
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BooksFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.nav_books));

        View view = inflater.inflate(R.layout.fragment_books, container, false);
        mContext = view.getContext();

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.btn_create_book);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CreateBookActivity.class);
                startActivityForResult(intent, 0);
            }
        });
        rvPrivateBook = (RecyclerView) view.findViewById(R.id.rv_private_books);
        rvPrivateBook.setLayoutManager(new LinearLayoutManager(mContext, OrientationHelper.VERTICAL, false));

        List<Book> books = BookORM.getBooks(mContext);

        // Set adapter to recycler views.
        mBooksRVAdapter = new BooksRVAdapter(books, mListener, mContext);
        rvPrivateBook.setAdapter(mBooksRVAdapter);
        registerForContextMenu(rvPrivateBook);
        return view;
    }

    private void openBook() {
        Intent intent = new Intent(getActivity(), BookContentActivity.class);
        intent.putExtra("BOOK_ID", mSelectedBook.getId());
        startActivity(intent);
    }

    private void studyBook() {
        Intent intent = new Intent(mContext, CreateStudyActivity.class);
        intent.putExtra("BOOK_ID", mSelectedBook.getId());
        startActivity(intent);
    }

    private void seeBookInfo() {
        Intent intent = new Intent(getActivity(), BookInfoActivity.class);
        intent.putExtra("BOOK_ID", mSelectedBook.getId());
        intent.putExtra("IS_MARKET", false);
        startActivity(intent);
    }

    private void editBook() {
        Intent intent = new Intent(mContext, CreateBookActivity.class);
        intent.putExtra("BOOK_ID", mSelectedBook.getId());
        startActivity(intent);
    }

    private void deleteBook() {
        new AlertDialog.Builder(mContext)
            .setTitle(mSelectedBook.getTitle())
            .setMessage("All related sections and vocabularies will be deleted. Delete this book? ")
            .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // continue with delete
                    if (new VocatDBManager(mContext).deleteBook(mSelectedBook.getId())) {
                        refreshList();
                    } else {
                        Toast.makeText(mContext, "Deletion failed", Toast.LENGTH_SHORT).show();
                    }
                }
            })
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // do nothing
                }
            })
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void refreshList() {
        mBooksRVAdapter.refreshItems(BookORM.getBooks(mContext));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (mSelectedBook == null) {
            return super.onContextItemSelected(item);
        }
        switch (item.getItemId()) {
            case R.id.ctx_menu_oepn_book:
                openBook();
                break;
            case R.id.ctx_menu_study_book:
                studyBook();
                break;
            case R.id.ctx_menu_book_info:
                seeBookInfo();
                break;
            case R.id.ctx_menu_edit_book:
                editBook();
                break;
            case R.id.ctx_menu_delete_book:
                deleteBook();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderIcon(R.drawable.ic_book);
        menu.setHeaderTitle(mSelectedBook.getTitle());
        menu.add(0, R.id.ctx_menu_oepn_book, 0, R.string.menu_open_book);
        menu.add(0, R.id.ctx_menu_study_book, 1, R.string.menu_study_book);
        menu.add(0, R.id.ctx_menu_book_info, 2, R.string.menu_book_info);
        menu.add(0, R.id.ctx_menu_edit_book, 3, R.string.menu_edit_book);
        menu.add(0, R.id.ctx_menu_delete_book, 4, R.string.menu_delete_book);
        if (mSelectedBook.getIsPublic())
            menu.findItem(R.id.ctx_menu_edit_book).setEnabled(false);
    }

    public interface OnListFragmentInteractionListener {
        void onListClick(Book book);
        void onListLongClick(Book book);
        void onSecondBtnClick(Book book);
    }
}
