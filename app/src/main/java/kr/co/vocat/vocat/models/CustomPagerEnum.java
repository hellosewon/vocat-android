package kr.co.vocat.vocat.models;

import kr.co.vocat.vocat.R;

public enum CustomPagerEnum {

    DUMMY_THIRD(R.string.toast_failed, R.layout.view_pager_main_ad, R.mipmap.ad3),
    FIRST(R.string.hint_create_book_title, R.layout.view_pager_main_ad, R.mipmap.ad1),
    SECOND(R.string.toast_failed, R.layout.view_pager_main_ad, R.mipmap.ad2),
    THIRD(R.string.txt_empty_stack, R.layout.view_pager_main_ad, R.mipmap.ad3),
    DUMMY_FIRST(R.string.toast_failed, R.layout.view_pager_main_ad, R.mipmap.ad1);

    private int mTitleResId;
    private int mLayoutResId;
    private int mBgResId;

    CustomPagerEnum(int titleResId, int layoutResId, int bgResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
        mBgResId = bgResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

    public int getBgResId() {
        return mBgResId;
    }

}