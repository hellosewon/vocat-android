package kr.co.vocat.vocat.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.android.volley.toolbox.Volley;

import java.util.HashMap;

import kr.co.vocat.toolbox.GsonRequest;
import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.adapters.BookMarketRVAdapter;
import kr.co.vocat.vocat.authenticator.VocatUserManager;
import kr.co.vocat.vocat.models.Book;


public class KnowhowFeedFragment extends Fragment {

    //private Book mSelectedBook;
   // private BookMarketRVAdapter mBookMarketRVAdapter;

    private Context mContext;
    private CoordinatorLayout mCoordinatorLayout;
    private RecyclerView rvKnowhowFeed;
    private LinearLayout mNoInternetLayout;
    private ScrollView mKnowhowFeedListLayout;
    private View mProgressView;
    private OnFragmentListInteractionListener mListener;

    public KnowhowFeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.nav_knowhowfeed));

        View view = inflater.inflate(R.layout.fragment_knowhow_feed, container, false);
        mContext = view.getContext();

        mCoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayout);
        mNoInternetLayout = (LinearLayout) view.findViewById(R.id.empty_no_internet);
        mKnowhowFeedListLayout = (ScrollView) view.findViewById(R.id.knowhow_feed_list);
        mProgressView = view.findViewById(R.id.knowhow_feed_progress);

        populateKnowhowFeed();
        return view;
    }

    private void populateKnowhowFeed() {
        if (!Utils.isNetworkAvailable(mContext)) {
            mNoInternetLayout.setVisibility(View.VISIBLE);
            mKnowhowFeedListLayout.setVisibility(View.GONE);
            Snackbar.make(mCoordinatorLayout,
                    R.string.snkbar_no_internet, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.snkbar_actn_retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            populateKnowhowFeed();
                        }
                    }).show();
        } else {
            //if(isAdded()) { showProgress(true); }
            mNoInternetLayout.setVisibility(View.GONE);
/*            String url = Utils.getAPIURL("book-market/");
            String token = mUserManager.getToken();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Token " + token);
            GsonRequest<Book[]> myReq = new GsonRequest<>(url,
                    Book[].class,
                    headers,
                    bookReqSuccessListener(),
                    bookReqErrorListener()
            );
            Volley.newRequestQueue(mContext).add(myReq);*/
        }

    }

    public interface OnFragmentListInteractionListener {
        void onFragmentListInteraction(Book book);
    }
}
