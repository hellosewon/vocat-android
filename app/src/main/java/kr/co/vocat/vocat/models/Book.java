package kr.co.vocat.vocat.models;

import java.util.Date;
import java.util.List;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class Book {
    private String id;
    private String date_created;
    private String date_updated;

    private String language_for;
    private String language_by;
    private String copy_of;
    private Integer creator;
    private Integer owner;
    private String title;
    private String description;
    private Boolean is_public;
    private List<Integer> likes = null;
    private List<Integer> dislikes = null;

    private Section[] sections;

    // For Book Market Displaying
    private long section_count;
    private long vocab_count;

    public Book() {

    }

    public Book(String id, String date_created, String date_updated, String language_for,
                String language_by, String copy_of, Integer creator, Integer owner,
                String title, String description, Boolean is_public) {
        this.id = id;
        this.date_created = date_created;
        this.date_updated = date_updated;
        this.language_for = language_for;
        this.language_by = language_by;
        this.copy_of = copy_of;
        this.creator = creator;
        this.owner = owner;
        this.title = title;
        this.description = description;
        this.is_public = is_public;
    }

    public String getId() { return this.id; }
    public String getDateCreated() { return this.date_created; }
    public String getDateUpdated() { return this.date_updated; }
    public String getLanguageFor() { return this.language_for; }
    public String getLanguageBy() { return this.language_by; }
    public String getCopyOf() { return this.copy_of; }
    public Integer getCreator() { return this.creator; }
    public Integer getOwner() { return this.owner; }
    public String getTitle() { return this.title; }
    public String getDescription() { return this.description; }
    public Boolean getIsPublic() { return this.is_public; }
    public Section[] getSections() { return this.sections; }
    //////////
    public long getSectionCount() { return this.section_count; }
    public long getVocabCount() { return this.vocab_count; }

    public void setId(String id) { this.id = id; }
    public void setDateCreated(String date_created) { this.date_created = date_created; }
    public void setDateUpdated(String date_updated) { this.date_updated = date_updated; }
    public void setLanguageFor(String language_for) { this.language_for = language_for; }
    public void setLanguageBy(String language_by) { this.language_by = language_by; }
    public void setCopyOf(String copy_of) { this.copy_of = copy_of; }
    public void setCreator(Integer creator) { this.creator = creator; }
    public void setOwner(Integer owner) { this.owner = owner; }
    public void setTitle(String title) { this.title = title; }
    public void setDescription(String description) { this.description = description; }
    public void setIsPublic(Boolean is_public) { this.is_public = is_public; }
    public void setSections(Section[] sections) { this.sections = sections; }
    ///////////
    public void setSectionCount(long section_count) { this.section_count = section_count; }
    public void setVocabCount(long vocab_count) { this.vocab_count = vocab_count; }
}
