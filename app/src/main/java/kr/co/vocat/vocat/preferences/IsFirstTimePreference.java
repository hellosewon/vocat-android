package kr.co.vocat.vocat.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import kr.co.vocat.vocat.Utils;

/**
 * Created by Shark on 2016-01-26.
 */
public class IsFirstTimePreference {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private static final String PREFER_NAME = Utils.PREF_NAME;
    private static final String KEY_IS_FIRST_TIME = "is_first_time";

    public IsFirstTimePreference(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
    }

    public boolean getIsFirstTime() {
        return pref.getBoolean(KEY_IS_FIRST_TIME, true);
    }

    public void setIsFirstTime(Boolean bool) {
        editor = pref.edit();
        editor.putBoolean(KEY_IS_FIRST_TIME, bool);
        editor.commit();
    }

    public void deleteIsFirstTime() {
        editor = pref.edit();
        editor.remove(KEY_IS_FIRST_TIME);
        editor.commit();
    }

}
