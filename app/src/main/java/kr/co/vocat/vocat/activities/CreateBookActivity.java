package kr.co.vocat.vocat.activities;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.authenticator.VocatUserManager;
import kr.co.vocat.vocat.data.VocatDBManager;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.data.orm.SectionORM;
import kr.co.vocat.vocat.models.Book;
import kr.co.vocat.vocat.models.Section;
import kr.co.vocat.vocat.models.User;
import kr.co.vocat.vocat.models.UserProfile;

public class CreateBookActivity extends AppCompatActivity {

    private String TAG = "CreateBookActivity";

    private Context mContext;
    private EditText mTitleView;
    private EditText mDescriptionView;

    private String mToken;
    private Book mBook;
    private Section mSection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_book);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = this;

        Button btnCancel = (Button) findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Button btnCreate = (Button) findViewById(R.id.btn_create);


        mTitleView = (EditText) findViewById(R.id.title);
        mDescriptionView = (EditText) findViewById(R.id.description);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            btnCreate.setText(R.string.create);
            btnCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createBook();
                }
            });
        } else {
            getSupportActionBar().setTitle(R.string.update_book);
            btnCreate.setText(R.string.update);
            final Book book = populateBookInfo(extras.getString("BOOK_ID"));
            btnCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateBook(book);
                }
            });
        }
        mTitleView.requestFocus();
    }

    private Book populateBookInfo(String bookId) {
        Book book = BookORM.findBookById(this, bookId);
        if (book != null) {
            mTitleView.setText(book.getTitle());
            mDescriptionView.setText(book.getDescription());
        }
        return book;
    }

    private boolean updateBook(Book book) {
        String title = mTitleView.getText().toString();
        if (!isTitleValid(title)) return false;
        String description = mDescriptionView.getText().toString();

        if (book.getTitle().equals(title) && book.getDescription().equals(description)) {
            return false;
        }
        book.setTitle(title);
        book.setDescription(description);
        book.setDateUpdated(Utils.getCurrentTimeStamp());
        if (new VocatDBManager(this).updateBook(book)) {
            Toast.makeText(mContext, R.string.toast_book_updated, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, R.string.toast_book_update_failed, Toast.LENGTH_SHORT).show();
        }
        finish();
        return true;
    }

    private boolean isTitleValid(String title) {
        if (title.equals("")) {
            Snackbar.make(mTitleView, R.string.snkbar_no_title, Snackbar.LENGTH_SHORT).show();
            mTitleView.requestFocus();
            return false;
        }
        return true;
    }

    private boolean createBook() {
        String title = mTitleView.getText().toString();
        if (!isTitleValid(title)) return false;
        String description = mDescriptionView.getText().toString();

        VocatDBManager dbManager = new VocatDBManager(this);
        Book book = dbManager.generateBook();
        book.setTitle(title);
        book.setDescription(description);
        if (dbManager.createBook(book)) {
            Toast.makeText(mContext, R.string.toast_book_created, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, R.string.toast_book_creation_failed, Toast.LENGTH_SHORT).show();
        }
        finish();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
