package kr.co.vocat.vocat.data.orm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kr.co.vocat.vocat.data.DatabaseHelper;
import kr.co.vocat.vocat.data.StudyManager;
import kr.co.vocat.vocat.models.Stack;
import kr.co.vocat.vocat.models.StudyVocab;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class StackORM {

    private static final String TAG = "StackORM";

    static final String TABLE_NAME = "stack";

    public static final String CREATE_TABLE = "create table "
            + TABLE_NAME + "("
            + "id integer primary key autoincrement not null, "
            + "study integer not null, "
            + "title varchar(255), "
            + "sort integer not null, "
            + "status integer not null, "
            + "check(title <> ''), "
            + "FOREIGN KEY(study) REFERENCES "+ StudyORM.TABLE_NAME + "(id) on delete cascade);";

    public static final String DROP_TABLE = "drop table if exists " + TABLE_NAME;

    public static List<Stack> getStacks(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Stack> stackList = null;

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + StackORM.TABLE_NAME, null);

            Log.i(TAG, "Loaded " + cursor.getCount() + " Stacks...");
            if (cursor.getCount() > 0) {
                stackList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Stack stack = cursorToStack(cursor);
                    stackList.add(stack);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Stacks loaded successfully.");
            }
            db.close();
        }
        return stackList;
    }

    public static List<Stack> getStacksByStatus(Context context, Integer status) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Stack> stackList = new ArrayList<>();

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + StackORM.TABLE_NAME
                    +" WHERE status = ?", new String[] { String.valueOf(status) });

            Log.i(TAG, "Loaded " + cursor.getCount() + " Stacks...");
            if (cursor.getCount() > 0) {
                stackList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Stack stack = cursorToStack(cursor);
                    stackList.add(stack);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Stacks loaded successfully.");
            }
            db.close();
        }
        return stackList;
    }

    public static List<Stack> getStacksByStatus(Context context, Integer status_from, Integer status_to) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Stack> stackList = new ArrayList<>();

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + StackORM.TABLE_NAME
                    +" WHERE status BETWEEN ? AND ?", new String[] { String.valueOf(status_from), String.valueOf(status_to) });

            Log.i(TAG, "Loaded " + cursor.getCount() + " Stacks...");
            if (cursor.getCount() > 0) {
                stackList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Stack stack = cursorToStack(cursor);
                    stackList.add(stack);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Stacks loaded successfully.");
            }
            db.close();
        }
        return stackList;
    }

    public static List<Stack> getStudyStacks(Context context, Integer studyId) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Stack> stackList = null;

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + StackORM.TABLE_NAME
                    +" WHERE study = ? ORDER BY sort ASC", new String[] { String.valueOf(studyId) });

            Log.i(TAG, "Loaded " + cursor.getCount() + " Stacks...");
            if (cursor.getCount() > 0) {
                stackList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Stack stack = cursorToStack(cursor);
                    stackList.add(stack);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Stacks loaded successfully.");
            }
            db.close();
        }
        return stackList;
    }

    public static Stack getStackToNotify(Context context, Integer studyId) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Stack stack = null;

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + StackORM.TABLE_NAME
                    +" WHERE study = ? AND status = ? ORDER BY sort ASC",
                    new String[] { String.valueOf(studyId), String.valueOf(StudyManager.UNDISCLOSED) });

            Log.i(TAG, "Loaded " + cursor.getCount() + " Stacks...");
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                stack = cursorToStack(cursor);
                Log.i(TAG, "Stack to notify loaded successfully.");
            }
            db.close();
        }
        return stack;
    }

    public static Stack findStackById(Context context, Integer id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Stack stack = null;
        if (db != null) {
            Log.i(TAG, "Loading Stack[" + id + "]...");
            Cursor cursor = db.rawQuery("SELECT * FROM " + StackORM.TABLE_NAME + " WHERE id = ?", new String[] { String.valueOf(id) });

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                stack = cursorToStack(cursor);
                Log.i(TAG, "Stack loaded successfully!");
            }
            db.close();
        }
        return stack;
    }

    public static boolean insertStack(Context context, Stack stack) {
        if (findStackById(context, stack.getId()) != null) {
            Log.i(TAG, "Stack already exists in database, not inserting!");
            return updateStack(context, stack);
        }
        ContentValues values = stackToContentValues(stack);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                long id = db.insertOrThrow(StackORM.TABLE_NAME, "null", values);
                Log.i(TAG, "Inserting Stack[" + id + "]...");
                for (StudyVocab studyVocab : stack.getStudyVocabs()) {
                    studyVocab.setStack((int) id);
                }
                success = StudyVocabORM.bulkInsertStudyVocab(context, stack.getStudyVocabs());
                //success = true;
            }
        } catch (SQLiteConstraintException ex) {
            Log.e(TAG, "Failed to insert Stack due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean updateStack(Context context, Stack stack) {
        ContentValues values = stackToContentValues(stack);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Updating Stack[" + stack.getId() + "]...");
                db.update(StackORM.TABLE_NAME, values, "id = ?", new String[]{String.valueOf(stack.getId())});
                success = true;
            }
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to update Stack[" + stack.getId() + "] due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean deleteStack(Context context, Integer id) {
        if (findStackById(context, id) == null) {
            Log.i(TAG, "Stack does not exist in database, not deleting!");
            return false;
        }
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        if (db != null) {
            Log.i(TAG, "Deleting Stack[" + id + "]...");
            int value = db.delete(StackORM.TABLE_NAME, "id = ?", new String[] { String.valueOf(id) });
            if (value > 0) {
                Log.i(TAG, "Stack[" + id + "] Deleted successfully!");
                success = true;
            }
            db.close();
        }
        return success;
    }

    public static long countEntireStack(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long stackCount = DatabaseUtils.queryNumEntries(db, StackORM.TABLE_NAME);
        db.close();
        return stackCount;
    }

    public static long countVocabulary(Context context, Integer id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long vocabCount = DatabaseUtils.queryNumEntries(db, StudyVocabORM.TABLE_NAME, "stack = ?", new String[] { String.valueOf(id) });
        db.close();
        return vocabCount;
    }

    private static ContentValues stackToContentValues(Stack stack) {
        ContentValues values = new ContentValues();
        //values.put("id", study.getId());
        values.put("study", stack.getStudy());
        values.put("title", stack.getTitle());
        values.put("sort", stack.getSort());
        values.put("status", stack.getStatus());
        return values;
    }

    private static Stack cursorToStack(Cursor cursor) {
        Stack stack = new Stack();

        int ci = cursor.getColumnIndex("id");
        if (!cursor.isNull(ci)) stack.setId(cursor.getInt(ci));
        ci = cursor.getColumnIndex("study");
        if (!cursor.isNull(ci)) stack.setStudy(cursor.getInt(ci));
        ci = cursor.getColumnIndex("title");
        if (!cursor.isNull(ci)) stack.setTitle(cursor.getString(ci));
        ci = cursor.getColumnIndex("sort");
        if (!cursor.isNull(ci)) stack.setSort(cursor.getInt(ci));
        ci = cursor.getColumnIndex("status");
        if (!cursor.isNull(ci)) stack.setStatus(cursor.getInt(ci));

        return stack;
    }
}
