//package kr.co.vocat.vocat.sync;
//
//import android.content.Context;
//import android.support.design.widget.Snackbar;
//import android.util.Log;
//
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.Volley;
//
//import java.util.HashMap;
//import java.util.List;
//
//import kr.co.vocat.toolbox.GsonRequest;
//import kr.co.vocat.vocat.Utils;
//import kr.co.vocat.vocat.authenticator.VocatUserManager;
//import kr.co.vocat.vocat.data.orm.BookORM;
//import kr.co.vocat.vocat.models.Book;
//
//*
// * Created by Shark on 2016-02-01.
// *
//
//
//public class VocatSyncManager {
//
//    private String TAG = "VocatSyncManager";
//    private Context mContext;
//
//    public VocatSyncManager(Context context) {
//        mContext = context;
//    }
//
//    public void syncNow() {
//        downloadBook();
//    }
//
//    private void downloadBook() {
//        String url = Utils.getAPIURL("books/");
//        VocatUserManager mUserManager = new VocatUserManager(mContext);
//        String token = mUserManager.getToken();
//        HashMap<String, String> headers = new HashMap<>();
//        headers.put("Authorization", "Token " + token);
//        GsonRequest<Book[]> myReq = new GsonRequest<>(url,
//                Book[].class,
//                headers,
//                bookReqSuccessListener(),
//                bookReqErrorListener()
//        );
//        Volley.newRequestQueue(mContext).add(myReq);
//    }
//
//    private Response.Listener<Book[]> bookReqSuccessListener() {
//        return new Response.Listener<Book[]>() {
//            @Override
//            public void onResponse(Book[] books) {
//                performSync(books);
//            }
//        };
//    }
//    private Response.ErrorListener bookReqErrorListener() {
//        return new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, error.getMessage());
//            }
//        };
//    }
//
//    private void performSync(Book[] serverBooks) {
//        List<Book> appBooks = BookORM.getBooks(mContext);
//
//        // App - Create or update book.
//        for (Book appBook : appBooks) {
//            for (Book serverBook : serverBooks) {
//                if (serverBook.getAppId() == null) {
//                    Integer app_id = BookORM.insertBook(mContext, serverBook);
//                    serverBook.setAppId(app_id);
//                    updateServerBook(serverBook);
//                } else if (serverBook.getAppId().equals(appBook.getAppId())) {
//                    int cmp = serverBook.getDateUpdated().compareTo(appBook.getDateUpdated());
//                    if (cmp == 0) {
//                        break;
//                    } else if (cmp > 0) {
//                        BookORM.insertBook(mContext, serverBook); // Updating
//                    }
//                }
//            }
//        }
//
//        // Server - Create or update book.
//        for (Book serverBook : serverBooks) {
//            for (Book appBook : appBooks) {
//                if (appBook.getServerId() == null) {
//                    //serverBook.createBook(appBook); // Make Request -> get serverId -> update bookorm
//                    BookORM.insertBook(mContext, serverBook); // on response?
//                } else if (appBook.getServerId().equals(serverBook.getServerId())) {
//                    int cmp = appBook.getDateUpdated().compareTo(serverBook.getDateUpdated());
//                    if (cmp == 0) {
//                        break;
//                    } else if (cmp > 0) {
//                        //Server.updateBook(appBook); // Make request to update book
//                        break;
//                    }
//                }
//            }
//        }
//
//        // App - Deleting Book
//    }
//
//    private void updateServerBook(Book serverBook) {
//        // Create
//        String url = Utils.getAPIURL("books/");
//        VocatUserManager mUserManager = new VocatUserManager(mContext);
//        String token = mUserManager.getToken();
//        HashMap<String, String> headers = new HashMap<>();
//        headers.put("Authorization", "Token " + token);
//        GsonRequest<Book[]> myReq = new GsonRequest<>(url,
//                Book[].class,
//                headers,
//                bookReqSuccessListener(),
//                bookReqErrorListener()
//        );
//        Volley.newRequestQueue(mContext).add(myReq);
//    }
//}
