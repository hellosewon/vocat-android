package kr.co.vocat.vocat.data.orm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kr.co.vocat.vocat.data.DatabaseHelper;
import kr.co.vocat.vocat.models.StudyVocab;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class StudyVocabORM {

    private static final String TAG = "StudyVocabORM";

    static final String TABLE_NAME = "study_vocab";

    public static final String CREATE_TABLE = "create table "
            + TABLE_NAME + "("
            + "id integer primary key autoincrement not null, "
            + "stack integer not null, "
            + "vocabulary varchar(255), "
            + "sort integer not null, "
            + "status integer not null, "
            + "FOREIGN KEY(stack) REFERENCES "+ StackORM.TABLE_NAME + "(id) on delete cascade, "
            + "FOREIGN KEY(vocabulary) REFERENCES "+ VocabularyORM.TABLE_NAME + "(id) on delete cascade);";

    public static final String DROP_TABLE = "drop table if exists " + TABLE_NAME;

    public static List<StudyVocab> getStudyVocabs(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<StudyVocab> studyVocabList = null;

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + StudyVocabORM.TABLE_NAME, null);

            Log.i(TAG, "Loaded " + cursor.getCount() + " StudyVocabs...");
            if (cursor.getCount() > 0) {
                studyVocabList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    StudyVocab studyVocab = cursorToStudyVocab(cursor);
                    studyVocabList.add(studyVocab);
                    cursor.moveToNext();
                }
                Log.i(TAG, "StudyVocabs loaded successfully.");
            }
            db.close();
        }
        return studyVocabList;
    }

    public static List<StudyVocab> getStackStudyVocabs(Context context, Integer stackId) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<StudyVocab> studyVocabList = null;

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + StudyVocabORM.TABLE_NAME
                    +" WHERE stack = ? ORDER BY sort ASC", new String[] { String.valueOf(stackId) });

            Log.i(TAG, "Loaded " + cursor.getCount() + " StudyVocabs...");
            if (cursor.getCount() > 0) {
                studyVocabList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    StudyVocab studyVocab = cursorToStudyVocab(cursor);
                    studyVocabList.add(studyVocab);
                    cursor.moveToNext();
                }
                Log.i(TAG, "StudyVocabs loaded successfully.");
            }
            db.close();
        }
        return studyVocabList;
    }

    public static StudyVocab findStudyVocabById(Context context, Integer id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        StudyVocab studyVocab = null;
        if (db != null) {
            Log.i(TAG, "Loading StudyVocab[" + id + "]...");
            Cursor cursor = db.rawQuery("SELECT * FROM " + StudyVocabORM.TABLE_NAME + " WHERE id = ?", new String[] { String.valueOf(id) });

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                studyVocab = cursorToStudyVocab(cursor);
                Log.i(TAG, "StudyVocab loaded successfully!");
            }
            db.close();
        }
        return studyVocab;
    }

    public static boolean insertStudyVocab(Context context, StudyVocab studyVocab) {
        if (findStudyVocabById(context, studyVocab.getId()) != null) {
            Log.i(TAG, "StudyVocab already exists in database, not inserting!");
            return updateStudyVocab(context, studyVocab);
        }
        ContentValues values = studyVocabToContentValues(studyVocab);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Inserting StudyVocab[" + studyVocab.getId() + "]...");
                db.insertOrThrow(StudyVocabORM.TABLE_NAME, "null", values);
                success = true;
            }
        } catch (SQLiteConstraintException ex) {
            Log.e(TAG, "Failed to insert StudyVocab due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean bulkInsertStudyVocab(Context context, List<StudyVocab> studyVocabs) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values;

        boolean success = false;
        try {
            db.beginTransaction();
            for (StudyVocab studyVocab : studyVocabs) {
                values = studyVocabToContentValues(studyVocab);
                db.insert(StudyVocabORM.TABLE_NAME, "null", values);
            }
            success = true;
            db.setTransactionSuccessful();
            Log.i(TAG, "Bulk Inserted StudyVocab successfully!");
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to bulk insert StudyVocab due to: " + ex);
        } finally {
            db.endTransaction();
        }
        db.close();
        return success;
    }

    public static boolean updateStudyVocab(Context context, StudyVocab studyVocab) {
        ContentValues values = studyVocabToContentValues(studyVocab);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Updating StudyVocab[" + studyVocab.getId() + "]...");
                db.update(StudyVocabORM.TABLE_NAME, values, "id = ?", new String[] { String.valueOf(studyVocab.getId()) });
                success = true;
            }
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to update StudyVocab[" + studyVocab.getId() + "] due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean deleteStudyVocab(Context context, Integer id) {
        if (findStudyVocabById(context, id) == null) {
            Log.i(TAG, "StudyVocab does not exist in database, not deleting!");
            return false;
        }
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        if (db != null) {
            Log.i(TAG, "Deleting StudyVocab[" + id + "]...");
            int value = db.delete(StudyVocabORM.TABLE_NAME, "id = ?", new String[] { String.valueOf(id) });
            if (value > 0) {
                Log.i(TAG, "StudyVocab[" + id + "] Deleted successfully!");
                success = true;
            }
            db.close();
        }
        return success;
    }

    public static long countEntireStudyVocab(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long studyVocabCount = DatabaseUtils.queryNumEntries(db, StudyVocabORM.TABLE_NAME);
        db.close();
        return studyVocabCount;
    }

    private static ContentValues studyVocabToContentValues(StudyVocab studyVocab) {
        ContentValues values = new ContentValues();
        //values.put("id", study.getId());
        values.put("stack", studyVocab.getStack());
        values.put("vocabulary", studyVocab.getVocabulary());
        values.put("sort", studyVocab.getSort());
        values.put("status", studyVocab.getStatus());
        return values;
    }

    private static StudyVocab cursorToStudyVocab(Cursor cursor) {
        StudyVocab studyVocab = new StudyVocab();

        int ci = cursor.getColumnIndex("id");
        if (!cursor.isNull(ci)) studyVocab.setId(cursor.getInt(ci));
        ci = cursor.getColumnIndex("stack");
        if (!cursor.isNull(ci)) studyVocab.setStack(cursor.getInt(ci));
        ci = cursor.getColumnIndex("vocabulary");
        if (!cursor.isNull(ci)) studyVocab.setVocabulary(cursor.getString(ci));
        ci = cursor.getColumnIndex("sort");
        if (!cursor.isNull(ci)) studyVocab.setSort(cursor.getInt(ci));
        ci = cursor.getColumnIndex("status");
        if (!cursor.isNull(ci)) studyVocab.setStatus(cursor.getInt(ci));

        return studyVocab;
    }
}
