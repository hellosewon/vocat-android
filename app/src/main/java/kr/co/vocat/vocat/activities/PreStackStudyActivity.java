package kr.co.vocat.vocat.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.data.StudyManager;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.data.orm.StackORM;
import kr.co.vocat.vocat.data.orm.StudyORM;
import kr.co.vocat.vocat.models.Book;
import kr.co.vocat.vocat.models.Stack;

public class PreStackStudyActivity extends AppCompatActivity {

    private Stack mStack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_stack_study);

        getStack();
        Button btnMemorizeStack = (Button) findViewById(R.id.btn_memorize_stack);
        Button btnCloseStack = (Button) findViewById(R.id.btn_close_stack);

        btnMemorizeStack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVocabDetailActivity(true);
            }
        });
        btnCloseStack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showVocabDetailActivity(true);
            }
        });
        Book book = BookORM.findBookById(this, StudyORM.findStudyById(this, mStack.getStudy()).getBook());
        ((TextView) findViewById(R.id.book_title)).setText(book.getTitle());
        ((TextView) findViewById(R.id.stack_title)).setText(mStack.getTitle());

    }

    private void getStack() {
        Bundle extras = getIntent().getExtras();
        Integer stackId = extras.getInt("STACK_ID");
        mStack = StackORM.findStackById(this, stackId);
    }

    private void showVocabDetailActivity(Boolean isSplit) {
        mStack.setStatus(StudyManager.STUDYING);
        StackORM.updateStack(this, mStack);

        Intent intent = new Intent(this, StackStudyActivity.class);
        intent.putExtra("STACK_ID", mStack.getId());
        intent.putExtra("IS_SPLIT", isSplit);
        startActivity(intent);
        finish();
    }
}
