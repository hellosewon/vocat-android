package kr.co.vocat.vocat.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.models.User;

/**
 * Created by Shark on 2016-01-26.
 */
public class UserPreference {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private static final String PREFER_NAME = Utils.PREF_NAME;
    private static final String KEY_ID = "id";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_NAME = "name";

    public UserPreference(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
    }

    public User getUser() {
        Integer id = pref.getInt(KEY_ID, -1);
        String email = pref.getString(KEY_EMAIL, null);
        String name = pref.getString(KEY_NAME, null);
        return new User(id, email, name);
    }

    public void setUser(User user) {
        editor = pref.edit();
        editor.putInt(KEY_ID, user.getId());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putString(KEY_NAME, user.getName());
        editor.commit();
    }

    public void deleteUser() {
        editor = pref.edit();
        editor.remove(KEY_ID);
        editor.remove(KEY_EMAIL);
        editor.remove(KEY_NAME);
        editor.commit();
    }
}
