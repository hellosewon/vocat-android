package kr.co.vocat.vocat.models;


import java.util.Date;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class Vocabulary {
    private String id;
    private String date_updated;

    private String section;
    private Integer sort;
    private String name;
    private String meaning;

    public Vocabulary() {

    }

    public Vocabulary(String id, String date_updated, String section,
                      Integer sort, String name, String meaning) {
        this.id = id;
        this.date_updated = date_updated;
        this.section = section;
        this.sort = sort;
        this.name = name;
        this.meaning = meaning;
    }

    public String getId() { return this.id; }
    public String getDateUpdated() { return this.date_updated; }
    public String getSection() { return this.section; }
    public Integer getSort() { return this.sort; }
    public String getName() { return this.name; }
    public String getMeaning() { return this.meaning; }

    public void setId(String id) { this.id = id; }
    public void setDateUpdated(String date_updated) { this.date_updated = date_updated; }
    public void setSection(String section) { this.section = section; }
    public void setSort(Integer sort) { this.sort = sort; }
    public void setName(String name) { this.name = name; }
    public void setMeaning(String meaning) { this.meaning = meaning; }
}
