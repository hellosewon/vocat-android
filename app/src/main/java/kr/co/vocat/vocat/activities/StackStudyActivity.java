package kr.co.vocat.vocat.activities;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

import kr.co.vocat.TTSManager;
import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.adapters.VocabSetPagerAdapter;
import kr.co.vocat.vocat.data.orm.StudyVocabORM;
import kr.co.vocat.vocat.models.StudyVocab;

public class StackStudyActivity extends AppCompatActivity {

    private Context mContext;
    private TTSManager ttsManager;
    ViewPager vpVocabSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stack_study);
        mContext = this;
        ttsManager = new TTSManager(mContext);

        Bundle extras = getIntent().getExtras();
        Integer stackId = extras.getInt("STACK_ID");
        Boolean isSplit = extras.getBoolean("IS_SPLIT");

        vpVocabSet = (ViewPager) findViewById(R.id.vp_vocab_set);
        vpVocabSet.setAdapter(new VocabSetPagerAdapter(getSupportFragmentManager(), mContext, stackId, isSplit));
    }

    private void updateStudyVocabStatus(List<StudyVocab> studyVocabs, Integer status) {
        for (StudyVocab studyVocab : studyVocabs) {
            studyVocab.setStatus(status);
            StudyVocabORM.updateStudyVocab(mContext, studyVocab);
        }
    }

    public void speakOut(String vocabulary) {
        ttsManager.speakOut(vocabulary);
    }

    public void showPrevSet() {
        vpVocabSet.setCurrentItem(vpVocabSet.getCurrentItem() - 1);
    }

    public void showNextSet() {
        vpVocabSet.setCurrentItem(vpVocabSet.getCurrentItem() + 1);
    }

}
