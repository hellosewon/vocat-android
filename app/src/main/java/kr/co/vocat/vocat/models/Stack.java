package kr.co.vocat.vocat.models;


import java.util.List;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class Stack {
    private Integer id;
    private Integer study;
    private String title;
    private Integer sort;
    private Integer status;

    private List<StudyVocab> studyVocabs;

    public Stack() {

    }

    public Stack(String title, Integer sort, Integer status) {
        this.title = title;
        this.sort = sort;
        this.status = status;
    }

    public Integer getId() { return this.id; }
    public Integer getStudy() { return this.study; }
    public String getTitle() { return this.title; }
    public Integer getSort() { return this.sort; }
    public Integer getStatus() { return this.status; }
    public List<StudyVocab> getStudyVocabs() { return this.studyVocabs; }

    public void setId(Integer id) { this.id = id; }
    public void setStudy(Integer study) { this.study = study; }
    public void setTitle(String title) { this.title = title; }
    public void setSort(Integer sort) { this.sort = sort; }
    public void setStatus(Integer status) { this.status = status; }
    public void setStudyVocabs(List<StudyVocab> studyVocabs) { this.studyVocabs = studyVocabs; }
}
