package kr.co.vocat.vocat.data.orm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kr.co.vocat.vocat.data.DatabaseHelper;
import kr.co.vocat.vocat.models.Section;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class SectionORM {

    private static final String TAG = "SectionORM";

    static final String TABLE_NAME = "section";

    public static final String CREATE_TABLE = "create table "
            + TABLE_NAME + "("
            + "id varchar(255) primary key, "
            + "date_updated datetime not null, "
            + "book varchar(255) not null, "
            + "type integer not null, "
            + "sort integer not null, "
            + "title varchar(255) not null, "
            + "description text not null, "
            + "check(title <> ''), "
            + "FOREIGN KEY(book) REFERENCES " + BookORM.TABLE_NAME + "(id) on delete cascade);";

    public static final String DROP_TABLE = "drop table if exists " + TABLE_NAME;


    public static List<Section> getSections(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Section> sectionList = null;

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + SectionORM.TABLE_NAME, null);

            Log.i(TAG, "Loaded " + cursor.getCount() + " Sections...");
            if (cursor.getCount() > 0) {
                sectionList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Section section = cursorToSection(cursor);
                    sectionList.add(section);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Sections loaded successfully.");
            }
            db.close();
        }
        return sectionList;
    }

    public static List<Section> getBookSections(Context context, String bookId) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Section> sectionList = null;

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + SectionORM.TABLE_NAME
                    +" WHERE book = ? ORDER BY sort ASC", new String[] { bookId });

            Log.i(TAG, "Loaded " + cursor.getCount() + " Sections...");
            if (cursor.getCount() > 0) {
                sectionList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Section section = cursorToSection(cursor);
                    sectionList.add(section);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Sections loaded successfully.");
            }
            db.close();
        }
        return sectionList;
    }


    public static Section findSectionById(Context context, String id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Section section = null;
        if (db != null) {
            Log.i(TAG, "Loading Section[" + id + "]...");
            Cursor cursor = db.rawQuery("SELECT * FROM " + SectionORM.TABLE_NAME + " WHERE id = ?", new String[] { id });

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                section = cursorToSection(cursor);
                Log.i(TAG, "Section loaded successfully!");
            }
            db.close();
        }
        return section;
    }

    public static boolean insertSection(Context context, Section section) {
        if (findSectionById(context, section.getId()) != null) {
            Log.i(TAG, "Section already exists in database, not inserting!");
            return updateSection(context, section);
        }
        ContentValues values = sectionToContentValues(section);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Inserting Section[" + section.getId() + "]...");
                db.insertOrThrow(SectionORM.TABLE_NAME, "null", values);
                success = true;
                if (section.getVocabularies() != null) success = VocabularyORM.bulkInsertVocabulary(context, section.getVocabularies());
            }
        } catch (SQLiteConstraintException ex) {
            Log.e(TAG, "Failed to insert Section due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean updateSection(Context context, Section section) {
        ContentValues values = sectionToContentValues(section);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Updating Section[" + section.getId() + "]...");
                db.update(SectionORM.TABLE_NAME, values, "id = ?", new String[] { section.getId() });
                success = true;
            }
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to update Section[" + section.getId() + "] due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean deleteSectionById(Context context, String id) {
        if (findSectionById(context, id) == null) {
            Log.i(TAG, "Section does not exist in database, not deleting!");
            return false;
        }
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        if (db != null) {
            Log.i(TAG, "Deleting Section[" + id + "]...");
            int value = db.delete(SectionORM.TABLE_NAME, "id = ?", new String[] { id });
            if (value > 0) {
                Log.i(TAG, "Section[" + id + "] Deleted successfully!");
                success = true;
            }
            db.close();
        }
        return success;
    }

    public static long countVocabulary(Context context, String id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long vocabCount = DatabaseUtils.queryNumEntries(db, VocabularyORM.TABLE_NAME, "section = ?", new String[] { id });
        db.close();
        return vocabCount;
    }

    private static ContentValues sectionToContentValues(Section section) {
        ContentValues values = new ContentValues();
        values.put("id", section.getId());
        values.put("date_updated", section.getDateUpdated());
        values.put("book", section.getBook());
        values.put("type", section.getType());
        values.put("sort", section.getSort());
        values.put("title", section.getTitle());
        values.put("description", section.getDescription());
        return values;
    }

    private static Section cursorToSection(Cursor cursor) {
        Section section = new Section();
        int ci = cursor.getColumnIndex("id");
        if (!cursor.isNull(ci)) section.setId(cursor.getString(ci));
        ci = cursor.getColumnIndex("date_updated");
        if (!cursor.isNull(ci)) section.setDateUpdated(cursor.getString(ci));
        ci = cursor.getColumnIndex("book");
        if (!cursor.isNull(ci)) section.setBook(cursor.getString(ci));
        ci = cursor.getColumnIndex("type");
        if (!cursor.isNull(ci)) section.setType(cursor.getInt(ci));
        ci = cursor.getColumnIndex("sort");
        if (!cursor.isNull(ci)) section.setSort(cursor.getInt(ci));
        ci = cursor.getColumnIndex("title");
        if (!cursor.isNull(ci)) section.setTitle(cursor.getString(ci));
        ci = cursor.getColumnIndex("description");
        if (!cursor.isNull(ci)) section.setDescription(cursor.getString(ci));

        return section;
    }
}
