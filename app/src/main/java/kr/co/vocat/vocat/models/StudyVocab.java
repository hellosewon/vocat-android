package kr.co.vocat.vocat.models;


/**
 * Created by Shark on 2016-01-30.
 *
 */
public class StudyVocab {
    private Integer id;
    private Integer stack;
    private String vocabulary;
    private Integer sort;
    private Integer status;

    public StudyVocab() {

    }

    public StudyVocab(String vocabulary, Integer sort, Integer status) {
        this.vocabulary = vocabulary;
        this.sort = sort;
        this.status = status;
    }

    public Integer getId() { return this.id; }
    public Integer getStack() { return this.stack; }
    public String getVocabulary() { return this.vocabulary; }
    public Integer getSort() { return this.sort; }
    public Integer getStatus() { return this.status; }

    public void setId(Integer id) { this.id = id; }
    public void setStack(Integer stack) { this.stack = stack; }
    public void setVocabulary(String vocabulary) { this.vocabulary = vocabulary; }
    public void setSort(Integer sort) { this.sort = sort; }
    public void setStatus(Integer status) { this.status = status; }
}
