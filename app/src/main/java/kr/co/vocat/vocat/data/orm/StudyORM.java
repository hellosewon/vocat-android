package kr.co.vocat.vocat.data.orm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kr.co.vocat.vocat.data.DatabaseHelper;
import kr.co.vocat.vocat.models.Stack;
import kr.co.vocat.vocat.models.Study;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class StudyORM {

    private static final String TAG = "StudyORM";

    static final String TABLE_NAME = "study";

    public static final String CREATE_TABLE = "create table "
            + TABLE_NAME + "("
            + "id integer primary key autoincrement not null, "
            + "date_created datetime not null, "
            + "book varchar(255) not null, "
            + "status integer not null, "
            //+ "stack_size integer not null, "
            + "FOREIGN KEY(book) REFERENCES "+ BookORM.TABLE_NAME + "(id) on delete cascade);";

    public static final String DROP_TABLE = "drop table if exists " + TABLE_NAME;

    public static List<Study> getStudies(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Study> studyList = new ArrayList<>();

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + StudyORM.TABLE_NAME, null);

            Log.i(TAG, "Loaded " + cursor.getCount() + " Studies...");
            if (cursor.getCount() > 0) {
                studyList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Study study = cursorToStudy(cursor);
                    studyList.add(study);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Studies loaded successfully.");
            }
            db.close();
        }
        return studyList;
    }

    public static List<Study> getStudiesByStatus(Context context, Integer status) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Study> studyList = new ArrayList<>();

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + StudyORM.TABLE_NAME
                    +" WHERE status = ?", new String[] { String.valueOf(status) });

            Log.i(TAG, "Loaded " + cursor.getCount() + " Studies...");
            if (cursor.getCount() > 0) {
                studyList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Study study = cursorToStudy(cursor);
                    studyList.add(study);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Studies loaded successfully.");
            }
            db.close();
        }
        return studyList;
    }

    public static Boolean doesBookStudyEixst(Context context, String bookId) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Boolean studyExist = false;
        if (db != null) {
            Log.i(TAG, "Searching Study of Book[" + bookId + "]...");
            Cursor cursor = db.rawQuery("SELECT * FROM " + StudyORM.TABLE_NAME + " WHERE book = ?", new String[] { String.valueOf(bookId) });

            if (cursor.getCount() > 0) {
                studyExist = true;
                Log.i(TAG, "Study exists!");
            }
            db.close();
        }
        return studyExist;
    }

    public static Study findStudyById(Context context, Integer id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Study study = null;
        if (db != null) {
            Log.i(TAG, "Loading Study[" + id + "]...");
            Cursor cursor = db.rawQuery("SELECT * FROM " + StudyORM.TABLE_NAME + " WHERE id = ?", new String[] { String.valueOf(id) });

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                study = cursorToStudy(cursor);
                Log.i(TAG, "Study loaded successfully!");
            }
            db.close();
        }
        return study;
    }

    public static boolean insertStudy(Context context, Study study) {
        if (findStudyById(context, study.getId()) != null) {
            Log.i(TAG, "Study already exists in database, not inserting!");
            return updateStudy(context, study);
        }
        ContentValues values = studyToContentValues(study);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                long id = db.insertOrThrow(StudyORM.TABLE_NAME, "null", values);
                Log.i(TAG, "Inserting Study[" + id + "]...");
                success = true;
                for (Stack stack : study.getStacks()) {
                    stack.setStudy((int) id);
                    if (!StackORM.insertStack(context, stack)) { success = false; break; }
                }
            }
        } catch (SQLiteConstraintException ex) {
            Log.e(TAG, "Failed to insert Study due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean updateStudy(Context context, Study study) {
        ContentValues values = studyToContentValues(study);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Updating Study[" + study.getId() + "]...");
                db.update(StudyORM.TABLE_NAME, values, "id = ?", new String[] { String.valueOf(study.getId()) });
                success = true;
            }
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to update Study[" + study.getId() + "] due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean deleteStudy(Context context, Integer id) {
        if (findStudyById(context, id) == null) {
            Log.i(TAG, "Study does not exist in database, not deleting!");
            return false;
        }
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        if (db != null) {
            Log.i(TAG, "Deleting Study[" + id + "]...");
            int value = db.delete(StudyORM.TABLE_NAME, "id = ?", new String[] { String.valueOf(id) });
            if (value > 0) {
                Log.i(TAG, "Study[" + id + "] Deleted successfully!");
                success = true;
            }
            db.close();
        }
        return success;
    }

    public static long countEntireStudy(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long studyCount = DatabaseUtils.queryNumEntries(db, StudyORM.TABLE_NAME);
        db.close();
        return studyCount;
    }

    private static ContentValues studyToContentValues(Study study) {
        ContentValues values = new ContentValues();
        //values.put("id", study.getId());
        values.put("date_created", study.getDateCreated());
        values.put("book", study.getBook());
        values.put("status", study.getStatus());
        return values;
    }

    private static Study cursorToStudy(Cursor cursor) {
        Study study = new Study();

        int ci = cursor.getColumnIndex("id");
        if (!cursor.isNull(ci)) study.setId(cursor.getInt(ci));
        ci = cursor.getColumnIndex("date_created");
        if (!cursor.isNull(ci)) study.setDateCreated(cursor.getString(ci));
        ci = cursor.getColumnIndex("book");
        if (!cursor.isNull(ci)) study.setBook(cursor.getString(ci));
        ci = cursor.getColumnIndex("status");
        if (!cursor.isNull(ci)) study.setStatus(cursor.getInt(ci));

        return study;
    }
}
