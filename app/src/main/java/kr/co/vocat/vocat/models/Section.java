package kr.co.vocat.vocat.models;


import java.util.Date;
import java.util.List;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class Section {
    private String id;
    private String date_updated;

    private String book;  // FK
    private Integer type;
    private Integer sort;
    private String title;
    private String description;

    private Vocabulary[] vocabularies;

    public Section() {

    }

    public Section(String id, String date_updated, String book, Integer type,
                   Integer sort, String title, String description) {
        this.id = id;
        this.date_updated = date_updated;
        this.book = book;
        this.type = type;
        this.sort = sort;
        this.title = title;
        this.description = description;
    }

    public String getId() { return this.id; }
    public String getDateUpdated() { return this.date_updated; }
    public String getBook() { return this.book; }
    public Integer getType() { return this.type; }
    public Integer getSort() { return this.sort; }
    public String getTitle() { return this.title; }
    public String getDescription() { return this.description; }
    public Vocabulary[] getVocabularies() { return this.vocabularies; }

    public void setId(String id) { this.id = id; }
    public void setDateUpdated(String date_updated) { this.date_updated = date_updated; }
    public void setBook(String book) { this.book = book; }
    public void setType(Integer type) { this.type = type; }
    public void setSort(Integer sort) { this.sort = sort; }
    public void setTitle(String title) { this.title = title; }
    public void setDescription(String description) { this.description = description; }
    public void setVocabularies(Vocabulary[] vocabularies) { this.vocabularies = vocabularies; }
}
