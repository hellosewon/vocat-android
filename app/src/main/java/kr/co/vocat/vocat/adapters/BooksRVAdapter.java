package kr.co.vocat.vocat.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.data.orm.SectionORM;
import kr.co.vocat.vocat.data.orm.StudyORM;
import kr.co.vocat.vocat.fragments.BooksFragment.OnListFragmentInteractionListener;
import kr.co.vocat.vocat.models.Book;

import java.util.List;


public class BooksRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private List<Book> mValues;
    private final OnListFragmentInteractionListener mListener;
    private final Context mContext;

    public BooksRVAdapter(List<Book> books, OnListFragmentInteractionListener listener, Context context) {
        mValues = books;
        mListener = listener;
        mContext = context;
    }

    public void refreshItems(List<Book> books) {
        mValues = books;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View emptyState = LayoutInflater.from (parent.getContext()).inflate(R.layout.empty_state, parent, false);
            View viewHeader = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_list_title, parent, false);
            LinearLayout view = (LinearLayout) viewHeader.findViewById(R.id.list_header);
            view.addView(emptyState);
            return new HeaderViewHolder(view);
        } else if(viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.two_line_item, parent, false);
            return new GenericViewHolder(view);
        } else if(viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from (parent.getContext()).inflate(R.layout.footer_dummy_list, parent, false);
            return new FooterViewHolder(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holderOriginal, int position) {
        if(holderOriginal instanceof HeaderViewHolder) {
            HeaderViewHolder holder = (HeaderViewHolder) holderOriginal;
            holder.mListTitle.setText(mContext.getResources().getString(R.string.shead_books));

            if (mValues.size() == 0) {
                holder.mEmptyState.setVisibility(View.VISIBLE);
                holder.mListTitle.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transparent));
            } else {
                holder.mEmptyState.setVisibility(View.GONE);
                holder.mListTitle.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            }

        } else if(holderOriginal instanceof GenericViewHolder) {
            final GenericViewHolder holder = (GenericViewHolder) holderOriginal;
            holder.mItem = mValues.get(position-1);
            holder.mAvatar.setImageResource(R.mipmap.img_book);
            holder.mPrimaryText.setText(holder.mItem.getTitle());
            holder.mSecondaryText.setText(holder.mItem.getDescription());
            long vocabCount = BookORM.countVocabulary(mContext, holder.mItem.getId());
            holder.mLittleInfo.setText(mContext.getString(R.string.txt_vocab_count, vocabCount));

            holder.mBtnSecondary.setImageResource(R.drawable.ic_school);
            if (StudyORM.doesBookStudyEixst(mContext, holder.mItem.getId())) {
                holder.mBtnSecondary.setColorFilter(ContextCompat.getColor(mContext, R.color.text_disabled_lightbg), android.graphics.PorterDuff.Mode.MULTIPLY);
                holder.mBtnSecondary.setAlpha(0.54f);
            } else {
                holder.mBtnSecondary.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
                holder.mBtnSecondary.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onSecondBtnClick(holder.mItem);
                    }
                });
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onListClick(holder.mItem);
                }
            });
            holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.onListLongClick(holder.mItem);
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemViewType (int position) {
        if(isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if(isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader (int position) {
        return position == 0;
    }

    private boolean isPositionFooter (int position) {
        return position == mValues.size() + 1;
    }

    @Override
    public int getItemCount() {
        return mValues.size() + 2;
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public final TextView mListTitle;
        public final View mEmptyState;

        public HeaderViewHolder (View view) {
            super (view);
            mListTitle = (TextView) view.findViewById(R.id.list_title);
            mEmptyState = view.findViewById(R.id.empty_state);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder (View view) {
            super (view);
        }
    }

    public class GenericViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mAvatar;
        public final TextView mPrimaryText;
        public final TextView mSecondaryText;
        public final TextView mLittleInfo;
        public final ImageButton mBtnSecondary;
        public Book mItem;

        public GenericViewHolder(View view) {
            super(view);
            mView = view;
            mAvatar = (ImageView) view.findViewById(R.id.left_avatar);
            mPrimaryText = (TextView) view.findViewById(R.id.primary_text);
            mSecondaryText = (TextView) view.findViewById(R.id.secondary_text);
            mLittleInfo = (TextView) view.findViewById(R.id.little_info);
            mBtnSecondary = (ImageButton) view.findViewById(R.id.btn_secondary);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mPrimaryText.getText() + "'";
        }
    }
}
