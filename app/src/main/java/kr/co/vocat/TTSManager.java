package kr.co.vocat;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Shark on 2016-03-11.
 *
 */
public class TTSManager {

    private Context mContext;
    private static TextToSpeech tts;
    private boolean readyToSpeak = false;

    public TTSManager(Context context) {
        mContext = context;
        tts = new TextToSpeech(mContext,  new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    } //else {  }
                    readyToSpeak = true;
                }
                else { Log.e("TTS", "Initilization Failed!"); }
            }
        });
    }

    public void speakOut(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (readyToSpeak) ttsGreater21(text);
        } else {
            if (readyToSpeak) ttsUnder20(text);
        }
    }
    @SuppressWarnings("deprecation")
    private static void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void ttsGreater21(String text) {
        //String utteranceId = this.hashCode() + "";
        String utteranceId = "si";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }


}
