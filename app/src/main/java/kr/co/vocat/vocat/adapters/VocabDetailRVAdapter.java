package kr.co.vocat.vocat.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.data.orm.VocabularyORM;
import kr.co.vocat.vocat.fragments.DashboardFragment.OnListFragmentInteractionListener;
import kr.co.vocat.vocat.fragments.VocabSetFragment;
import kr.co.vocat.vocat.models.Book;
import kr.co.vocat.vocat.models.StudyVocab;
import kr.co.vocat.vocat.models.Vocabulary;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Book} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class VocabDetailRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private List<StudyVocab> mValues;
    private VocabSetFragment.OnVocabSetFragmentInteractionListener mListener;
    private final Context mContext;
    private int mPage;
    private int mStackSize;

    public VocabDetailRVAdapter(List<StudyVocab> studyVocab, int page, int stackSize,
                                VocabSetFragment.OnVocabSetFragmentInteractionListener listener,
                                Context context) {
        mValues = studyVocab;
        mListener = listener;
        mContext = context;
        mPage = page;
        mStackSize = stackSize;
    }

    public void refreshItems(List<StudyVocab> studyVocab) {
        mValues = studyVocab;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View viewHeader = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_vocab_detail, parent, false);
            //LinearLayout v = (LinearLayout) viewHeader.findViewById(R.id.header_vocab_detail);
            return new HeaderViewHolder(viewHeader);
        } else if(viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_view_vocab_detail, parent, false);
            return new GenericViewHolder(view);
        } else if(viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from (parent.getContext()).inflate(R.layout.footer_vocab_detail, parent, false);
            return new FooterViewHolder(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holderOriginal, int position) {
        if(holderOriginal instanceof HeaderViewHolder) {
            HeaderViewHolder holder = (HeaderViewHolder) holderOriginal;
            holder.mSetTitle.setText(mContext.getString(R.string.shead_set, mPage));
            int setSize = mPage*mValues.size();
            String txtProgress;
            if (setSize <= mStackSize) txtProgress = String.valueOf(setSize);
            else txtProgress = String.valueOf(mStackSize);
            txtProgress += "/" + String.valueOf(mStackSize);
            holder.mSetProgress.setText(txtProgress);
            holder.mBtnPrevSet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onBtnPrevSetClick();
                }
            });
            holder.mBtnNextSet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onBtnNextSetClick();
                }
            });
        } else if(holderOriginal instanceof GenericViewHolder) {
            final GenericViewHolder holder = (GenericViewHolder) holderOriginal;
            holder.mItem = mValues.get(position-1);
            final Vocabulary vocabulary = VocabularyORM.findVocabularyById(mContext, holder.mItem.getVocabulary());
            holder.mOrder.setText(String.valueOf(holder.mItem.getSort()));
            holder.mName.setText(vocabulary.getName());
            holder.mMeaning.setText(vocabulary.getMeaning());
            holder.mPronunciation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onBtnPronunciationClick(vocabulary.getName());
                }
            });
        } else if(holderOriginal instanceof FooterViewHolder) {
            FooterViewHolder holder = (FooterViewHolder) holderOriginal;
            holder.mBtnShuffle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onBtnMixClick();
                }
            });
        }
    }

    @Override
    public int getItemViewType (int position) {
        if(isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if(isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader (int position) {
        return position == 0;
    }

    private boolean isPositionFooter (int position) {
        return position == mValues.size() + 1;
    }

    @Override
    public int getItemCount() {
        return mValues.size() + 2;
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public final TextView mSetTitle;
        public final TextView mSetProgress;
        public final Button mBtnPrevSet;
        public final Button mBtnNextSet;

        public HeaderViewHolder(View view) {
            super (view);
            mSetTitle = (TextView) view.findViewById(R.id.shead_set);
            mSetProgress = (TextView) view.findViewById(R.id.set_progress);
            mBtnPrevSet = (Button) view.findViewById(R.id.btn_previous_set);
            mBtnNextSet = (Button) view.findViewById(R.id.btn_next_set);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        public final Button mBtnShuffle;

        public FooterViewHolder (View view) {
            super (view);
            mBtnShuffle = (Button) view.findViewById(R.id.btn_shuffle);
        }

    }

    public class GenericViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mOrder;
        public final TextView mName;
        public final TextView mMeaning;
        public final ImageButton mPronunciation;
        public StudyVocab mItem;

        public GenericViewHolder(View view) {
            super(view);
            mView = view;
            mOrder = (TextView) view.findViewById(R.id.vocab_order);
            mName = (TextView) view.findViewById(R.id.vocab_name);
            mMeaning = (TextView) view.findViewById(R.id.vocab_meaning);
            mPronunciation = (ImageButton) view.findViewById(R.id.btn_pronunciation);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mName.getText() + "'";
        }
    }
}
