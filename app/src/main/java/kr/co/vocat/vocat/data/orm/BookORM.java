package kr.co.vocat.vocat.data.orm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kr.co.vocat.vocat.data.DatabaseHelper;
import kr.co.vocat.vocat.models.Book;
import kr.co.vocat.vocat.models.Section;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class BookORM {

    private static final String TAG = "BookORM";

    static final String TABLE_NAME = "book";

    public static final String CREATE_TABLE = "create table "
            + TABLE_NAME + "("
            + "id varchar(255) primary key, "
            + "date_created datetime not null, "
            + "date_updated datetime not null, "
            + "language_for varchar(2) not null, "
            + "language_by varchar(2) not null, "
            + "copy_of integer, "
            + "creator integer, "
            + "owner integer, "
            + "title varchar(255) not null, "
            + "description text not null, "
            + "is_public boolean not null, "
            + "check(title <> '')); ";

    public static final String DROP_TABLE = "drop table if exists " + TABLE_NAME;


    public static List<Book> getBooks(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Book> bookList = new ArrayList<>();

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + BookORM.TABLE_NAME, null);

            Log.i(TAG, "Loaded " + cursor.getCount() + " Books...");
            if (cursor.getCount() > 0) {
                bookList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Book book = cursorToBook(cursor);
                    bookList.add(book);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Books loaded successfully.");
            }
            db.close();
        }
        return bookList;
    }


    public static Book findBookById(Context context, String id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Book book = null;
        if (db != null) {
            Log.i(TAG, "Loading Book[" + id + "]...");
            Cursor cursor = db.rawQuery("SELECT * FROM " + BookORM.TABLE_NAME + " WHERE id = ?", new String[] { id });

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                book = cursorToBook(cursor);
                Log.i(TAG, "Book loaded successfully!");
            }
            db.close();
        }
        return book;
    }

    public static boolean insertBook(Context context, Book book) {
        if (findBookById(context, book.getId()) != null) {
            Log.i(TAG, "Book already exists in database, not inserting!");
            return updateBook(context, book);
        }
        ContentValues values = bookToContentValues(book);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Inserting Book[" + book.getId() + "]...");
                db.insertOrThrow(BookORM.TABLE_NAME, "null", values);
                success = true;
                Section[] sections = book.getSections();
                if (sections != null) {
                    for (Section section : sections) {
                        if (!SectionORM.insertSection(context, section)) { success = false; break; }
                    }
                }
            }
        } catch (SQLiteConstraintException ex) {
            Log.e(TAG, "Failed to insert Book due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean updateBook(Context context, Book book) {
        ContentValues values = bookToContentValues(book);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Updating Book[" + book.getId() + "]...");
                db.update(BookORM.TABLE_NAME, values, "id = ?", new String[] { book.getId() });
                success = true;
            }
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to update Book[" + book.getId() + "] due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean deleteBookById(Context context, String id) {
        if (findBookById(context, id) == null) {
            Log.i(TAG, "Book does not exist in database, not deleting!");
            return false;
        }
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        if (db != null) {
            Log.i(TAG, "Deleting Book[" + id + "]...");
            int value = db.delete(BookORM.TABLE_NAME, "id = ?", new String[] { id });
            if (value > 0) {
                Log.i(TAG, "Book[" + id + "] Deleted successfully!");
                success = true;
            }
            db.close();
        }
        return success;
    }

    public static long countEntireBook(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long bookCount = DatabaseUtils.queryNumEntries(db, BookORM.TABLE_NAME);
        db.close();
        return bookCount;
    }

    public static long countSection(Context context, String id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long sectionCount = DatabaseUtils.queryNumEntries(db, SectionORM.TABLE_NAME, "book = ?", new String[] { id });
        db.close();
        return sectionCount;
    }

    public static long countVocabulary(Context context, String id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor mCount = db.rawQuery("SELECT COUNT(*) FROM " + VocabularyORM.TABLE_NAME
                + " WHERE section IN "
                + "(SELECT id FROM " + SectionORM.TABLE_NAME
                + " WHERE book = ?)", new String[] { id });
        mCount.moveToFirst();
        long vocabCount = mCount.getInt(0);
        mCount.close();
        db.close();
        return vocabCount;
    }

    private static ContentValues bookToContentValues(Book book) {
        ContentValues values = new ContentValues();
        values.put("id", book.getId());
        values.put("date_created", book.getDateCreated());
        values.put("date_updated", book.getDateUpdated());
        values.put("language_for", book.getLanguageFor());
        values.put("language_by", book.getLanguageBy());
        values.put("copy_of", book.getCopyOf());
        values.put("creator", book.getCreator());
        values.put("owner", book.getOwner());
        values.put("title", book.getTitle());
        values.put("description", book.getDescription());
        values.put("is_public", book.getIsPublic());
        return values;
    }

    private static Book cursorToBook(Cursor cursor) {
        Book book = new Book();
        int ci = cursor.getColumnIndex("id");
        if (!cursor.isNull(ci)) book.setId(cursor.getString(ci));
        ci = cursor.getColumnIndex("date_created");
        if (!cursor.isNull(ci)) book.setDateCreated(cursor.getString(ci));
        ci = cursor.getColumnIndex("date_updated");
        if (!cursor.isNull(ci)) book.setDateUpdated(cursor.getString(ci));
        ci = cursor.getColumnIndex("language_for");
        if (!cursor.isNull(ci)) book.setLanguageFor(cursor.getString(ci));
        ci = cursor.getColumnIndex("language_by");
        if (!cursor.isNull(ci)) book.setLanguageBy(cursor.getString(ci));
        ci = cursor.getColumnIndex("copy_of");
        if (!cursor.isNull(ci)) book.setCopyOf(cursor.getString(ci));
        ci = cursor.getColumnIndex("creator");
        if (!cursor.isNull(ci)) book.setCreator(cursor.getInt(ci));
        ci = cursor.getColumnIndex("owner");
        if (!cursor.isNull(ci)) book.setOwner(cursor.getInt(ci));
        ci = cursor.getColumnIndex("title");
        if (!cursor.isNull(ci)) book.setTitle(cursor.getString(ci));
        ci = cursor.getColumnIndex("description");
        if (!cursor.isNull(ci)) book.setDescription(cursor.getString(ci));
        book.setIsPublic(cursor.getInt(cursor.getColumnIndex("is_public")) > 0);

        return book;
    }
}
