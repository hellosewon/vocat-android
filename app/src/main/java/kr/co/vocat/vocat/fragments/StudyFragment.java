package kr.co.vocat.vocat.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import kr.co.vocat.toolbox.MyLinearLayoutManager;
import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.adapters.StudyRVAdapter;
import kr.co.vocat.vocat.data.orm.StudyORM;
import kr.co.vocat.vocat.models.Study;


public class StudyFragment extends Fragment {

    private Context mContext;
    private RecyclerView rvStudy;
    private Study mSelectedStudy;
    private StudyRVAdapter mStudyRVAdapter;
    private OnListFragmentInteractionListener mListener  = new OnListFragmentInteractionListener() {
        @Override
        public void onListClick(Study study) {
            mSelectedStudy = study;
            //openBook();
        }
        @Override
        public void onListLongClick(Study study) {
            mSelectedStudy = study;
            rvStudy.showContextMenu();
        }
    };

    public StudyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.nav_study));
        View view = inflater.inflate(R.layout.fragment_study, container, false);
        mContext = view.getContext();

        rvStudy = (RecyclerView) view.findViewById(R.id.rv_study);
        rvStudy.setLayoutManager(new LinearLayoutManager(mContext, OrientationHelper.VERTICAL, false));

        List<Study> studies = StudyORM.getStudies(mContext);

        // Set adapter to recycler views.
        mStudyRVAdapter = new StudyRVAdapter(studies, mListener, mContext);
        rvStudy.setAdapter(mStudyRVAdapter);
        registerForContextMenu(rvStudy);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void refreshList() {
        mStudyRVAdapter.refreshItems(StudyORM.getStudies(mContext));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (mSelectedStudy == null) {
            return super.onContextItemSelected(item);
        }
        switch (item.getItemId()) {
            case R.id.ctx_menu_oepn_book:
                //openBook();
                break;
            case R.id.ctx_menu_study_book:
                //studyBook();
                break;
            case R.id.ctx_menu_book_info:
                //seeBookInfo();
                break;
            case R.id.ctx_menu_edit_book:
                //editBook();
                break;
            case R.id.ctx_menu_delete_book:
                //downloadBook();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderIcon(R.drawable.ic_book);
        menu.setHeaderTitle(mSelectedStudy.getBook());
//        menu.add(0, R.id.ctx_menu_oepn_book, 0, R.string.menu_open_book);
//        menu.add(0, R.id.ctx_menu_study_book, 1, R.string.menu_study_book);
//        menu.add(0, R.id.ctx_menu_book_info, 2, R.string.menu_book_info);
//        menu.add(0, R.id.ctx_menu_edit_book, 3, R.string.menu_edit_book);
        menu.add(0, R.id.ctx_menu_delete_book, 4, R.string.menu_delete_book);
        menu.findItem(R.id.ctx_menu_delete_book).setEnabled(false);

    }

    public interface OnListFragmentInteractionListener {
        void onListClick(Study study);
        void onListLongClick(Study study);
        //void onSecondBtnClick(Study study);
    }
}
