package kr.co.vocat.vocat.data.orm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kr.co.vocat.vocat.data.DatabaseHelper;
import kr.co.vocat.vocat.models.Vocabulary;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class VocabularyORM {

    private static final String TAG = "VocabularyORM";

    static final String TABLE_NAME = "vocabulary";

    public static final String CREATE_TABLE = "create table "
            + TABLE_NAME + "("
            + "id varchar(255) primary key, "
            + "date_updated datetime not null, "
            + "section varchar(255) not null, "
            + "sort integer not null, "
            + "name varchar(255) not null, "
            + "meaning text not null, "
            + "check(name <> ''), "
            + "FOREIGN KEY(section) REFERENCES "+ SectionORM.TABLE_NAME + "(id) on delete cascade);";

    public static final String DROP_TABLE = "drop table if exists " + TABLE_NAME;

    public static List<Vocabulary> getVocabularies(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Vocabulary> vocabularyList = null;

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT * FROM " + VocabularyORM.TABLE_NAME, null);

            Log.i(TAG, "Loaded " + cursor.getCount() + " Vocabularies...");
            if (cursor.getCount() > 0) {
                vocabularyList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Vocabulary vocabulary = cursorToVocabulary(cursor);
                    vocabularyList.add(vocabulary);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Vocabularies loaded successfully.");
            }
            db.close();
        }
        return vocabularyList;
    }

    public static List<Vocabulary> getBookVocabularies(Context context, String bookId) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        List<Vocabulary> vocabularyList = null;

        if (db != null) {
            Cursor cursor = db.rawQuery("SELECT vocabulary.*, section.sort AS section_sort FROM " + VocabularyORM.TABLE_NAME
                    + " INNER JOIN section ON vocabulary.section=section.id"
                    + " WHERE section IN"
                    + " (SELECT id FROM " + SectionORM.TABLE_NAME
                    + " WHERE book = ?) ORDER BY section_sort ASC, sort ASC", new String[] { bookId });

            Log.i(TAG, "Loaded " + cursor.getCount() + " Vocabularies...");
            if (cursor.getCount() > 0) {
                vocabularyList = new ArrayList<>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Vocabulary vocabulary = cursorToVocabulary(cursor);
                    vocabularyList.add(vocabulary);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Vocabularies loaded successfully.");
            }
            db.close();
        }
        return vocabularyList;
    }

    public static Vocabulary findVocabularyById(Context context, String id) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Vocabulary vocabulary = null;
        if (db != null) {
            Log.i(TAG, "Loading Vocabulary[" + id + "]...");
            Cursor cursor = db.rawQuery("SELECT * FROM " + VocabularyORM.TABLE_NAME + " WHERE id = ?", new String[] { id });

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                vocabulary = cursorToVocabulary(cursor);
                Log.i(TAG, "Vocabulary loaded successfully!");
            }
            db.close();
        }
        return vocabulary;
    }

    public static boolean insertVocabulary(Context context, Vocabulary vocabulary) {
        if (findVocabularyById(context, vocabulary.getId()) != null) {
            Log.i(TAG, "Vocabulary already exists in database, not inserting!");
            return updateVocabulary(context, vocabulary);
        }
        ContentValues values = vocabularyToContentValues(vocabulary);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Inserting Vocabulary[" + vocabulary.getId() + "]...");
                db.insertOrThrow(VocabularyORM.TABLE_NAME, "null", values);
                success = true;
            }
        } catch (SQLiteConstraintException ex) {
            Log.e(TAG, "Failed to insert Vocabulary due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean bulkInsertVocabulary(Context context, Vocabulary[] vocabularies) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values;

        boolean success = false;
        try {
            db.beginTransaction();
            for (Vocabulary vocabulary : vocabularies) {
                values = vocabularyToContentValues(vocabulary);
                db.insert(VocabularyORM.TABLE_NAME, "null", values);
            }
            success = true;
            db.setTransactionSuccessful();
            Log.i(TAG, "Bulk Inserted Vocabulary successfully!");
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to bulk insert Vocabulary due to: " + ex);
        } finally {
            db.endTransaction();
        }
        db.close();
        return success;
    }

    public static boolean updateVocabulary(Context context, Vocabulary vocabulary) {
        ContentValues values = vocabularyToContentValues(vocabulary);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        try {
            if (db != null) {
                Log.i(TAG, "Updating Vocabulary[" + vocabulary.getId() + "]...");
                db.update(VocabularyORM.TABLE_NAME, values, "id = ?", new String[] { vocabulary.getId() });
                success = true;
            }
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to update Vocabulary[" + vocabulary.getId() + "] due to: " + ex);
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return success;
    }

    public static boolean deleteVocabulary(Context context, String id) {
        if (findVocabularyById(context, id) == null) {
            Log.i(TAG, "Vocabulary does not exist in database, not deleting!");
            return false;
        }
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        boolean success = false;
        if (db != null) {
            Log.i(TAG, "Deleting Vocabulary[" + id + "]...");
            int value = db.delete(VocabularyORM.TABLE_NAME, "id = ?", new String[] { id });
            if (value > 0) {
                Log.i(TAG, "Vocabulary[" + id + "] Deleted successfully!");
                success = true;
            }
            db.close();
        }
        return success;
    }

    public static long countEntireVocabulary(Context context) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long vocabCount = DatabaseUtils.queryNumEntries(db, VocabularyORM.TABLE_NAME);
        db.close();
        return vocabCount;
    }

    private static ContentValues vocabularyToContentValues(Vocabulary vocabulary) {
        ContentValues values = new ContentValues();
        values.put("id", vocabulary.getId());
        values.put("date_updated", vocabulary.getDateUpdated());
        values.put("section", vocabulary.getSection());
        values.put("sort", vocabulary.getSort());
        values.put("name", vocabulary.getName());
        values.put("meaning", vocabulary.getMeaning());
        return values;
    }

    private static Vocabulary cursorToVocabulary(Cursor cursor) {
        Vocabulary vocabulary = new Vocabulary();

        int ci = cursor.getColumnIndex("id");
        if (!cursor.isNull(ci)) vocabulary.setId(cursor.getString(ci));
        ci = cursor.getColumnIndex("date_updated");
        if (!cursor.isNull(ci)) vocabulary.setDateUpdated(cursor.getString(ci));
        ci = cursor.getColumnIndex("section");
        if (!cursor.isNull(ci)) vocabulary.setSection(cursor.getString(ci));
        ci = cursor.getColumnIndex("sort");
        if (!cursor.isNull(ci)) vocabulary.setSort(cursor.getInt(ci));
        ci = cursor.getColumnIndex("name");
        if (!cursor.isNull(ci)) vocabulary.setName(cursor.getString(ci));
        ci = cursor.getColumnIndex("meaning");
        if (!cursor.isNull(ci)) vocabulary.setMeaning(cursor.getString(ci));

        return vocabulary;
    }
}
