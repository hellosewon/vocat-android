package kr.co.vocat.vocat.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.activities.PreStackStudyActivity;
import kr.co.vocat.vocat.adapters.MainAdPagerAdapter;
import kr.co.vocat.vocat.adapters.StacksRVAdapter;
import kr.co.vocat.vocat.data.StudyManager;
import kr.co.vocat.vocat.models.Stack;


public class DashboardFragment extends Fragment {

    private static final String TAG = "DashboardFragment";

    private Context mContext;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private MainAdPagerAdapter mPagerAdapter;
    private ViewPager vpMainAd;

    private RecyclerView rvNotifiedStacks;
    private Stack mSelectedStack;
    private StacksRVAdapter mStacksRVAdapter;
    private OnListFragmentInteractionListener mListener = new OnListFragmentInteractionListener() {
        @Override
        public void onListClick(Stack stack) {
            mSelectedStack = stack;
            openPreStudyStack();
        }
        @Override
        public void onListLongClick(Stack stack) {
            mSelectedStack = stack;
            rvNotifiedStacks.showContextMenu();
        }
    };

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.nav_dashboard));
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        mContext = view.getContext();

        // Main Ad ViewPager
        vpMainAd = (ViewPager) view.findViewById(R.id.vp_main_ad);
        pager_indicator = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);
        setUpMainAdVP();
        setUiPageViewController();

        // Dashboard Stack Notification
        final StudyManager studyManager = new StudyManager(mContext);

        rvNotifiedStacks = (RecyclerView) view.findViewById(R.id.rv_notified_stacks);
        rvNotifiedStacks.setLayoutManager(new LinearLayoutManager(mContext, OrientationHelper.VERTICAL, false));

        List<Stack> stacks = studyManager.getNotifiedStacks();

        // Set adapter to recycler views.
        mStacksRVAdapter = new StacksRVAdapter(stacks, mListener, mContext);
        rvNotifiedStacks.setAdapter(mStacksRVAdapter);
        registerForContextMenu(rvNotifiedStacks);

        ImageButton btn6AM = (ImageButton) view.findViewById(R.id.btn_6am);
        btn6AM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                do6AM();
            }
        });
        return view;
    }

    private void openPreStudyStack() {
        Intent intent = new Intent(getActivity(), PreStackStudyActivity.class);
        intent.putExtra("STACK_ID", mSelectedStack.getId());
        startActivity(intent);
    }

    // TODO: finishStack Activity
    private void openFinishStack() {
/*        Intent intent = new Intent(getActivity(), PreStackStudyActivity.class);
        intent.putExtra("STACK_ID", mSelectedStack.getId());
        startActivity(intent);*/
    }

    private void do6AM() {
        StudyManager studyManager = new StudyManager(mContext);
        studyManager.notifyNextStacks();
        mStacksRVAdapter.refreshItems(studyManager.getNotifiedStacks());
        Toast.makeText(mContext, "Notified next stacks!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (mSelectedStack == null) {
            return super.onContextItemSelected(item);
        }
        switch (item.getItemId()) {
            case R.id.ctx_menu_memorize_stack:
                openPreStudyStack();
                break;
            case R.id.ctx_menu_finish_stack:
                openFinishStack();
                break;
            case R.id.ctx_menu_study_info:
                //seeBookInfo();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderIcon(R.drawable.ic_book);
        menu.setHeaderTitle(mSelectedStack.getTitle());
        menu.add(0, R.id.ctx_menu_memorize_stack, 0, R.string.btn_memorize_stack);
        menu.add(0, R.id.ctx_menu_finish_stack, 1, R.string.btn_finish_stack);
        // menu.add(0, R.id.ctx_menu_study_info, 2, R.string.menu_study_info);
        // menu.findItem(R.id.ctx_menu_study_info).setEnabled(false);
    }

    private void setUpMainAdVP() {
        mPagerAdapter = new MainAdPagerAdapter(mContext);
        vpMainAd.setAdapter(mPagerAdapter);
        vpMainAd.setCurrentItem(1, false);
        vpMainAd.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.nonselecteditem_dot));
                }
                if (position == 0) position = vpMainAd.getAdapter().getCount() - 3;
                else if (position == vpMainAd.getAdapter().getCount() - 1) position = 0;
                else position -= 1;
                dots[position].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.selecteditem_dot));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    int curr = vpMainAd.getCurrentItem();
                    int lastReal = vpMainAd.getAdapter().getCount() - 2;
                    if (curr == 0) {
                        vpMainAd.setCurrentItem(lastReal, false);
                    } else if (curr > lastReal) {
                        vpMainAd.setCurrentItem(1, false);
                    }
                }
            }
        });
    }

    private void setUiPageViewController() {
        dotsCount = mPagerAdapter.getCount() - 2;
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(mContext);
            dots[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.selecteditem_dot));
    }

    public interface OnListFragmentInteractionListener {
        void onListClick(Stack stack);
        void onListLongClick(Stack stack);
    }
}
