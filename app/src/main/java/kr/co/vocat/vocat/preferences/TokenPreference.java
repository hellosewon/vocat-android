package kr.co.vocat.vocat.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import kr.co.vocat.vocat.Utils;

/**
 * Created by Shark on 2016-01-25.
 */
public class TokenPreference {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private static final String PREFER_NAME = Utils.PREF_NAME;
    private static final String KEY_TOKEN = "token";
    private static final String KEY_DEVICE_ID = "device_id";

    public TokenPreference(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
    }

    public String getToken() {
        return pref.getString(KEY_TOKEN, null);
    }

    public void setToken(String token) {
        editor = pref.edit();
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public void deleteToken() {
        editor = pref.edit();
        editor.remove(KEY_TOKEN);
        editor.commit();
    }

    public Integer getDeviceId() {
        return pref.getInt(KEY_DEVICE_ID, -1);
    }

    public void setDeviceId(Integer deviceId) {
        editor = pref.edit();
        editor.putInt(KEY_DEVICE_ID, deviceId);
        editor.commit();
    }

    public void deleteDeviceId() {
        editor = pref.edit();
        editor.remove(KEY_DEVICE_ID);
        editor.commit();
    }

}
