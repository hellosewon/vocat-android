package kr.co.vocat.vocat.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Shark on 2016-01-25.
 */
public class UserProfile {

    private int id;
    private int user;
    private String sex;
    private String date_of_birth;
    private String language_for;
    private String language_by;

    public UserProfile(JSONObject jObject) {
        try {
            this.id = jObject.getInt("id");
            this.user = jObject.getJSONObject("user").getInt("id");
            this.sex = jObject.getString("sex");
            this.date_of_birth = jObject.getString("date_of_birth");
            this.language_for = jObject.getString("language_for");
            this.language_by = jObject.getString("language_by");
        } catch (JSONException e) {
            Log.e("Shark", "UserProfile model JSONException!");
        }
    }

    public UserProfile(String sex, String date_of_birth, String language_for, String language_by) {
        this.sex = sex;
        this.date_of_birth = date_of_birth;
        this.language_for = language_for;
        this.language_by = language_by;
    }

    public int getId() {
        return this.id;
    }

    public int getUser() {
        return this.user;
    }

    public String getSex() {
        return this.sex;
    }

    public String getDateOfBirth() {
        return this.date_of_birth;
    }

    public String getLanguageFor() {
        return this.language_for;
    }

    public String getLanguageBy() {
        return this.language_by;
    }
}
