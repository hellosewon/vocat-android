package kr.co.vocat.vocat.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import kr.co.vocat.toolbox.MyLinearLayoutManager;
import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.adapters.SectionsRVAdapter;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.data.orm.SectionORM;
import kr.co.vocat.vocat.models.Book;
import kr.co.vocat.vocat.models.Section;

public class BookContentActivity extends AppCompatActivity {

    private Context mContext;
    private RecyclerView rvSections;

    private SectionsRVAdapter mSectionsRVAdapter;
    private Section mSelectedSection;

    private OnListFragmentInteractionListener mListener = new OnListFragmentInteractionListener() {
        @Override
        public void onListClick(Section section) {
            //mSelectedSection = section;
            //openSection(book.getId());
            Toast.makeText(mContext, "Clicked!", Toast.LENGTH_SHORT).show();
        }
        @Override
        public void onListLongClick(Section section) {
            mSelectedSection = section;
            rvSections.showContextMenu();
        }
        @Override
        public void onSecondBtnClick(Book book) {
            studyBook(book);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_content);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = this;

        rvSections = (RecyclerView) findViewById(R.id.rv_sections);
        rvSections.setLayoutManager(new LinearLayoutManager(this, OrientationHelper.VERTICAL, false));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btn_create_section);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(this, CreateSectionActivity.class);
//                startActivityForResult(intent, 0);
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Book book = BookORM.findBookById(this, extras.getString("BOOK_ID"));
            List<Section> sections = SectionORM.getBookSections(mContext, book.getId());
            mSectionsRVAdapter = new SectionsRVAdapter(book, sections, mListener, this);
            rvSections.setAdapter(mSectionsRVAdapter);
            registerForContextMenu(rvSections);
            if (book.getIsPublic()) fab.setVisibility(View.GONE);
        }
    }

    private void studyBook(Book book) {
        Intent intent = new Intent(mContext, CreateStudyActivity.class);
        intent.putExtra("BOOK_ID", book.getId());
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
/*        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (mSelectedSection == null) {
            return super.onContextItemSelected(item);
        }
        switch (item.getItemId()) {
            case R.id.ctx_menu_book_info:
                //seeBookInfo();
                break;
            case R.id.ctx_menu_oepn_book:
                //openBook();
                break;
            case R.id.ctx_menu_download_book:
                //downloadBook();
                break;
        }
        return super.onContextItemSelected(item);
    }

    //TODO: Context Menu for sections
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderIcon(R.drawable.ic_book);
        menu.setHeaderTitle(mSelectedSection.getTitle());
        menu.add(0, R.id.ctx_menu_oepn_section, 0, R.string.menu_open_section);
        menu.add(0, R.id.ctx_menu_study_section, 1, R.string.menu_study_section);
        menu.add(0, R.id.ctx_menu_section_info, 2, R.string.menu_section_info);
        menu.add(0, R.id.ctx_menu_edit_section, 3, R.string.menu_edit_section);
        menu.add(0, R.id.ctx_menu_delete_section, 4, R.string.menu_delete_section);
    }

    public interface OnListFragmentInteractionListener {
        void onListClick(Section section);
        void onListLongClick(Section section);
        void onSecondBtnClick(Book book);
    }

}
