package kr.co.vocat.vocat.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import kr.co.vocat.vocat.data.StudyVocabManager;
import kr.co.vocat.vocat.fragments.VocabSetFragment;
import kr.co.vocat.vocat.models.StudyVocab;

public class VocabSetPagerAdapter extends FragmentPagerAdapter {
    private int SET_SIZE = 10;
    private int mPageCount;
    private Context mContext;
    private Integer mStackId;
    private Boolean mIsSplit;

    public VocabSetPagerAdapter(FragmentManager fm, Context context, Integer stackId, Boolean isSplit) {
        super(fm);
        mContext = context;
        mStackId = stackId;
        mIsSplit = isSplit;
        mPageCount = getPageCount();
    }

    @Override
    public int getCount() {
        return mPageCount;
    }

    @Override
    public Fragment getItem(int position) {
        return VocabSetFragment.newInstance(position + 1, mIsSplit, mStackId);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "SET" + String.valueOf(position+1);
    }

    private int getPageCount() {
        StudyVocabManager vocabManager = new StudyVocabManager(mContext);
        List<StudyVocab> stackVocabs = vocabManager.getStackStudyVocabs(mStackId);
        int vocabSize = stackVocabs.size();
        int pageCount = vocabSize / SET_SIZE;
        if (vocabSize % SET_SIZE > 0) pageCount += 1;
        return pageCount;
    }

}