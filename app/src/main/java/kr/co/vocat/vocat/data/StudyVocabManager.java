package kr.co.vocat.vocat.data;

import android.content.Context;

import java.util.List;

import kr.co.vocat.vocat.data.orm.StackORM;
import kr.co.vocat.vocat.data.orm.StudyORM;
import kr.co.vocat.vocat.data.orm.StudyVocabORM;
import kr.co.vocat.vocat.models.Stack;
import kr.co.vocat.vocat.models.Study;
import kr.co.vocat.vocat.models.StudyVocab;

/**
 * Created by Shark on 2016-02-14.
 *
 */
public class StudyVocabManager {

    public static final int UNMEMORIZED = -1;  // Not completed memorizing
    public static final int STATUS_0 = 0;  // Memorized but not auto-reviewing
    public static final int STATUS_1 = 1;  // Auto-review in 1 day.
    public static final int STATUS_2 = 2;  // Auto-review in 3 days.
    public static final int STATUS_3 = 3;  // Auto-review in 7 days.
    public static final int STATUS_4 = 4;  // Auto-review in 14 days.
    public static final int STATUS_5 = 5;  // Not Auto-reviewing

    private Context mContext;

    public StudyVocabManager(Context context) {
        mContext = context;
    }

//    public void notifyNextStacks() {
//        List<Study> activeStudies = StudyORM.getStudiesByStatus(mContext, STUDYING);
//        for (Study study : activeStudies) {
//            Stack stack = StackORM.getStackToNotify(mContext, study.getId());
//            if (stack != null) {
//                stack.setStatus(NOTIFIED);
//                StackORM.updateStack(mContext, stack);
//            }
//        }
//    }

    public List<StudyVocab> getStackStudyVocabs(Integer stackId) {
        //return StudyVocabORM.getStudyVocabsByStatus(mContext, NOTIFIED);
        return StudyVocabORM.getStackStudyVocabs(mContext, stackId);
    }

}
