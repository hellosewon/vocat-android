package kr.co.vocat.vocat.activities;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.authenticator.VocatUserManager;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private String TAG = "LoginActivity";
    // Global variable
    private Context mContext;
    private String mEmail;
    private String mPassword;

    // UI references.
    private CoordinatorLayout mCoordinatorLayout;
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Button mEmailSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        mEmailView = (EditText) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin(); // if enter key is pressed
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        mContext = this;
        retrieveIntent();
    }

    private void retrieveIntent() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String email = extras.getString("EMAIL");
            String password = extras.getString("PASSWORD");
            mEmailView.setText(email);
            mPasswordView.setText(password);
            mEmailSignInButton.setFocusable(true);
            mEmailSignInButton.setFocusableInTouchMode(true);
            mEmailSignInButton.requestFocus();
            mEmailSignInButton.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
            Snackbar.make(mCoordinatorLayout,
                    R.string.snkbar_now_sign_in, Snackbar.LENGTH_INDEFINITE).show();
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        mEmail = email;
        mPassword = password;

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !Utils.isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Utils.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            if (!Utils.isNetworkAvailable(this)) {
                Snackbar.make(mCoordinatorLayout,
                        R.string.snkbar_no_internet, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.snkbar_actn_retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                attemptLogin();
                            }
                        }).show();
            } else {
                mEmailSignInButton.setEnabled(false);
                showProgress(true);
                login(email, password);
            }
        }
    }
    private void login(String email, String password) {
        String url = Utils.getAPIURL("accounts/login/");
        HashMap<String, String> myParams = new HashMap<>();
        myParams.put("username", email);
        myParams.put("password", password);
        myParams.put("type", "1");
        JsonObjectRequest myReq = new JsonObjectRequest(Method.POST,
                url,
                new JSONObject(myParams),
                reqSuccessListener(),
                reqErrorListener()
        );
        Volley.newRequestQueue(this).add(myReq);
    }

    private Response.Listener<JSONObject> reqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                postResponseAction();
                try {
                    String token = response.getString("token");
                    Integer deviceId = response.getInt("device_id");
                    new VocatUserManager(mContext).setTD(token, deviceId); // Saving token and deviceId
                    Intent intent = new Intent(mContext, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    //kill_activity();
                } catch (JSONException e) {
                    Snackbar.make(mCoordinatorLayout,
                            R.string.snkbar_cant_login, Snackbar.LENGTH_SHORT).show();
                }
            }
        };
    }

    private Response.ErrorListener reqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                postResponseAction();
                if (error.networkResponse == null) {
                    Snackbar.make(mCoordinatorLayout,
                            R.string.snkbar_server_no_response, Snackbar.LENGTH_SHORT).show();
                } else {
                    switch (error.networkResponse.statusCode) {
                        case 400:  // Bad Request. mal-formed
                            Snackbar.make(mCoordinatorLayout,
                                    R.string.snkbar_bad_req, Snackbar.LENGTH_SHORT).show();
                            break;
                        case 403:  // Forbidden. Password incorrect.
                            Snackbar.make(mCoordinatorLayout,
                                    R.string.snkbar_incorrect_pw, Snackbar.LENGTH_INDEFINITE)
                                    .setAction(R.string.snkbar_actn_forgot_pw, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(mContext, PasswordResetActivity.class);
                                            intent.putExtra("EMAIL", mEmail);
                                            startActivity(intent);
                                        }
                                    })
                                    .show();
                            break;
                        case 404:  // Not Found. So, register.
                            Snackbar.make(mCoordinatorLayout,
                                    R.string.snkbar_ru_new_user, Snackbar.LENGTH_INDEFINITE)
                                    .setAction(R.string.snkbar_actn_signup, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            // Show RegistrationActivity
                                            Intent intent = new Intent(mContext, RegistrationActivity.class);
                                            intent.putExtra("EMAIL", mEmail);
                                            intent.putExtra("PASSWORD", mPassword);
                                            startActivity(intent);
                                            kill_activity();
                                        }
                                    })
                                    .show();
                            break;
                        case 500:  // Internal Server Error.
                            Snackbar.make(mCoordinatorLayout,
                                    R.string.snkbar_server_error, Snackbar.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        };
    }

    private void postResponseAction() {
        showProgress(false);
        mEmailSignInButton.setEnabled(true);
        mPasswordView.requestFocus();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
//            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
//
//            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
//                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//                }
//            });
//
//            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//            mProgressView.animate().setDuration(shortAnimTime).alpha(
//                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//                }
//            });
//        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//        }
    }

    private void kill_activity() {
        finish();
    }

}

