package kr.co.vocat.vocat.models;


import java.util.List;

/**
 * Created by Shark on 2016-01-30.
 *
 */
public class Study {
    private Integer id;
    private String date_created;
    private String book;
    private Integer status;

    private List<Stack> stacks;

    public Study() {

    }

    public Study(String date_created, String book, Integer status) {
        this.date_created = date_created;
        this.book = book;
        this.status = status;
    }

    public Integer getId() { return this.id; }
    public String getDateCreated() { return this.date_created; }
    public String getBook() { return this.book; }
    public Integer getStatus() { return this.status; }
    public List<Stack> getStacks() { return this.stacks; }

    public void setId(Integer id) { this.id = id; }
    public void setDateCreated(String date_created) { this.date_created = date_created; }
    public void setBook(String book) { this.book = book; }
    public void setStatus(Integer status) { this.status = status; }
    public void setStacks(List<Stack> stacks) { this.stacks = stacks; }
}
