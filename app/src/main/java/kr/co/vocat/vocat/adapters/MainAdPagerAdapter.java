package kr.co.vocat.vocat.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.models.CustomPagerEnum;

public class MainAdPagerAdapter extends PagerAdapter {

    private Context mContext;

    public MainAdPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(customPagerEnum.getLayoutResId(), collection, false);

        ImageView background = (ImageView) layout.findViewById(R.id.img_ad_bg);
        TextView type = (TextView) layout.findViewById(R.id.ad_type);
        TextView title = (TextView) layout.findViewById(R.id.ad_title);
        TextView littleInfo = (TextView) layout.findViewById(R.id.little_info);

        background.setImageResource(customPagerEnum.getBgResId());
        type.setText("");
        title.setText("");
        littleInfo.setText("");

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return CustomPagerEnum.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }

}