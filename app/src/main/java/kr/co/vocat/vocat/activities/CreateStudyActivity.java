package kr.co.vocat.vocat.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.data.StudyManager;
import kr.co.vocat.vocat.data.StudyVocabManager;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.data.orm.StudyORM;
import kr.co.vocat.vocat.data.orm.VocabularyORM;
import kr.co.vocat.vocat.models.Book;
import kr.co.vocat.vocat.models.Stack;
import kr.co.vocat.vocat.models.Study;
import kr.co.vocat.vocat.models.StudyVocab;
import kr.co.vocat.vocat.models.Vocabulary;

public class CreateStudyActivity extends AppCompatActivity {

    private Context mContext;
    private Book mBook;
    private long mVocabCount;

    private int mExpectedDays;
    private int mStackSize;

    private LinearLayout mMainLayout;
    private ProgressBar mProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_study);

        mContext = this;

        Bundle extras = getIntent().getExtras();
        mBook = BookORM.findBookById(this, extras.getString("BOOK_ID"));
        mVocabCount = BookORM.countVocabulary(this, mBook.getId());

        mMainLayout = (LinearLayout) findViewById(R.id.layout_study_main);
        mProgressView = (ProgressBar) findViewById(R.id.create_study_progress);
        TextView bookTitle = (TextView) findViewById(R.id.book_title);
        bookTitle.setText(mBook.getTitle());
        final RadioGroup rgStudyOrder = (RadioGroup) findViewById(R.id.studyOrderOption);

        Button btnCancel = (Button) findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        Button btnCreate = (Button) findViewById(R.id.btn_create);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //setResult(Activity.RESULT_CANCELED);
                //createStudy();

                int studyOrder = rgStudyOrder.getCheckedRadioButtonId();
                new createStudyTask(mContext).execute(studyOrder);
            }
        });

        NumberPicker np = (NumberPicker) findViewById(R.id.npStackSize);

        String[] minuteValues = new String[15];
        for (int i=0;i<minuteValues.length;i++) {
            String number = Integer.toString((i+1)*10);
            minuteValues[i] = getString(R.string.np_x_words, number);
        }
        np.setMinValue(1);
        np.setMaxValue(minuteValues.length);
        np.setDisplayedValues(minuteValues);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                changeInfoText(newVal);
            }
        });
        np.setValue(3);
        changeInfoText(3);
    }

    private void changeInfoText(int newVal) {
        TextView info = (TextView) findViewById(R.id.infoExpectedDays);
        mStackSize = newVal * 10;
        int vocabCount = (int) mVocabCount;
        mExpectedDays = vocabCount / mStackSize;
        if (vocabCount % mStackSize > 0) mExpectedDays += 1;

        info.setText(getString(R.string.txt_expected_days, mExpectedDays));
    }


    private class createStudyTask extends AsyncTask<Integer, Void, Boolean> {

        private Context mContext;

        public createStudyTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            showProgress(true);
            Snackbar.make(mMainLayout, "Creating...", Snackbar.LENGTH_INDEFINITE).show();
        }

        @Override
        protected Boolean doInBackground(Integer... params) {

            Integer studyOrder = params[0];
            List<Vocabulary> vocabularyList = VocabularyORM.getBookVocabularies(mContext, mBook.getId());

            if (studyOrder == R.id.random) {
                Collections.shuffle(vocabularyList);
            }

            List<Stack> stackList = new ArrayList<>();
            for (int i=0; i<mExpectedDays; i++) {
                String stackTitle = "Day " + String.valueOf(i+1) + " of " + String.valueOf(mExpectedDays); //TODO: String resource
                Integer stackStatus = (i==0) ? StudyManager.NOTIFIED : StudyManager.UNDISCLOSED;
                Stack stack = new Stack(stackTitle, i, stackStatus);
                List<StudyVocab> studyVocabList = new ArrayList<>();
                for (int j=0; j<mStackSize; j++) {
                    if (vocabularyList.size() == 0) break;
                    Vocabulary vocabulary = vocabularyList.remove(0);
                    //Log.d("SHARK", "Sort=" + String.valueOf(vocabulary.getSort()) + ", vocab=" + vocabulary.getName());
                    StudyVocab studyVocab = new StudyVocab(vocabulary.getId(), j+1, StudyVocabManager.UNMEMORIZED);
                    studyVocabList.add(studyVocab);
                }
                stack.setStudyVocabs(studyVocabList);
                stackList.add(stack);
            }
            Study study = new Study(Utils.getCurrentTimeStamp(), mBook.getId(), StudyManager.STUDYING);
            study.setStacks(stackList);
            return StudyORM.insertStudy(mContext, study);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            showProgress(false);
            if (success) {
                Toast.makeText(mContext, "Study created.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mMainLayout.setVisibility(show ? View.GONE : View.VISIBLE);
            mMainLayout.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMainLayout.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mMainLayout.setVisibility(show ? View.GONE : View.VISIBLE);
        }

    }
}
