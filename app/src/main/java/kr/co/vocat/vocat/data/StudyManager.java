package kr.co.vocat.vocat.data;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import kr.co.vocat.vocat.data.orm.StackORM;
import kr.co.vocat.vocat.data.orm.StudyORM;
import kr.co.vocat.vocat.models.Stack;
import kr.co.vocat.vocat.models.Study;

/**
 * Created by Shark on 2016-02-14.
 */
public class StudyManager {

    public static final int UNDISCLOSED = -1;  // For Stack
    public static final int NOTIFIED = 0;  // For Stack
    public static final int STUDYING = 1;  // For both Study and Stack
    public static final int FINISHED = 2;  // For both Study and Stack

    private Context mContext;

    public StudyManager(Context context) {
        mContext = context;
    }

    public void notifyNextStacks() {
        List<Study> activeStudies = StudyORM.getStudiesByStatus(mContext, STUDYING);
        for (Study study : activeStudies) {
            Stack stack = StackORM.getStackToNotify(mContext, study.getId());
            if (stack != null) {
                stack.setStatus(NOTIFIED);
                StackORM.updateStack(mContext, stack);
            }
        }
    }

    public List<Stack> getNotifiedStacks() {
        return StackORM.getStacksByStatus(mContext, NOTIFIED, STUDYING);
    }

}
