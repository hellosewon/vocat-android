package kr.co.vocat.vocat.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import kr.co.vocat.vocat.Utils;

/**
 * Created by Shark on 2016-01-26.
 */
public class CountPreference {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private static final String PREFER_NAME = Utils.PREF_NAME;
    private static final String KEY_BOOK_COUNT = "book_count";
    private static final String KEY_SECTION_COUNT = "section_count";
    private static final String KEY_VOCABULARY_COUNT = "vocabulary_count";

    public CountPreference(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
    }

    public Integer getBookCount() { return pref.getInt(KEY_BOOK_COUNT, -1); }

    public void setBookCount(Integer bookCount) {
        editor = pref.edit();
        editor.putInt(KEY_BOOK_COUNT, bookCount);
        editor.commit();
    }

    public void deleteBookCount() {
        editor = pref.edit();
        editor.remove(KEY_BOOK_COUNT);
        editor.commit();
    }

    public Integer getSectionCount() { return pref.getInt(KEY_SECTION_COUNT, -1); }

    public void setSectionCount(Integer sectionCount) {
        editor = pref.edit();
        editor.putInt(KEY_SECTION_COUNT, sectionCount);
        editor.commit();
    }

    public void deleteSectionCount() {
        editor = pref.edit();
        editor.remove(KEY_SECTION_COUNT);
        editor.commit();
    }

    public Integer getVocabularyCount() { return pref.getInt(KEY_VOCABULARY_COUNT, -1); }

    public void setVocabularyCount(Integer vocabularyCount) {
        editor = pref.edit();
        editor.putInt(KEY_VOCABULARY_COUNT, vocabularyCount);
        editor.commit();
    }

    public void deleteVocabularyCount() {
        editor = pref.edit();
        editor.remove(KEY_VOCABULARY_COUNT);
        editor.commit();
    }

    // Knowhow

    public void deleteAllCount() {
        editor = pref.edit();
        editor.remove(KEY_BOOK_COUNT);
        editor.remove(KEY_SECTION_COUNT);
        editor.remove(KEY_VOCABULARY_COUNT);
        editor.commit();
    }
}
