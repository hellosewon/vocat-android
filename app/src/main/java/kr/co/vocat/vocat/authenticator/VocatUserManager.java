package kr.co.vocat.vocat.authenticator;

import android.content.Context;
import android.content.SharedPreferences;

import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.models.User;
import kr.co.vocat.vocat.models.UserProfile;
import kr.co.vocat.vocat.preferences.CountPreference;
import kr.co.vocat.vocat.preferences.IsFirstTimePreference;
import kr.co.vocat.vocat.preferences.TokenPreference;
import kr.co.vocat.vocat.preferences.UserPreference;
import kr.co.vocat.vocat.preferences.UserProfilePreference;

/**
 * Created by Shark on 2016-01-25.
 */
public class VocatUserManager {

    private Context mContext;
    private TokenPreference tokenPref;
    private UserPreference userPref;
    private UserProfilePreference userProfilePref;
    private IsFirstTimePreference isFirstTimePref;
    private CountPreference countPref;

    public VocatUserManager(Context context) {
        mContext = context;
        tokenPref = new TokenPreference(context);
        userPref = new UserPreference(context);
        userProfilePref = new UserProfilePreference(context);
        isFirstTimePref = new IsFirstTimePreference(context);
        countPref = new CountPreference(context);
    }

    public void logout() {
        // Delete all preferences(Token, User, UserProfile, IsFirstTime)
        SharedPreferences prefs = mContext.getSharedPreferences(Utils.PREF_NAME, 0);
        prefs.edit().clear().commit();
        // Delete all Database
        mContext.deleteDatabase(Utils.DATABASE_NAME);
    }

    public void clearAllPrefExceptTD() {
        // tokenPref.deleteToken();
        isFirstTimePref.deleteIsFirstTime();
        userPref.deleteUser();
        userProfilePref.deleteUserProfile();
        countPref.deleteAllCount();
    }

    public boolean isLoggedIn() {
        // Check if token exists and return boolean
        if (tokenPref.getToken() == null) return false;
        return true;
    }

    public boolean isFirstTime() { return isFirstTimePref.getIsFirstTime(); }

    public void setIsFirstTime(boolean isFirstTime) { isFirstTimePref.setIsFirstTime(isFirstTime); }

    public void setTD(String token, Integer deviceId) {
        tokenPref.setToken(token);
        tokenPref.setDeviceId(deviceId);
    }

    public void setToken(String token) { tokenPref.setToken(token); }

    public String getToken() { return tokenPref.getToken(); }

    public void setDeviceId(Integer deviceId) { tokenPref.setDeviceId(deviceId); }

    public Integer getDeviceId() { return tokenPref.getDeviceId(); }

    public void setUser(User user) { userPref.setUser(user); }

    public User getUser() { return userPref.getUser(); }

    public void setUserProfile(UserProfile userProfile) {
        userProfilePref.setUserProfile(userProfile);
    }

    public UserProfile getUserProfile() { return userProfilePref.getUserProfile(); }

    public void setAllCount(Integer bookCount, Integer sectionCount, Integer vocabularyCount) {
        countPref.setBookCount(bookCount);
        countPref.setSectionCount(sectionCount);
        countPref.setVocabularyCount(vocabularyCount);
    }

    public void setBookCount(Integer bookCount) { countPref.setBookCount(bookCount); }

    public Integer getBookCount() { return countPref.getBookCount(); }

    public void setSectionCount(Integer sectionCount) { countPref.setSectionCount(sectionCount); }

    public Integer getSectionCount() { return countPref.getSectionCount(); }

    public void setVocabularyCount(Integer vocabularyCount) { countPref.setVocabularyCount(vocabularyCount); }

    public Integer getVocabularyCount() { return countPref.getVocabularyCount(); }
}
