package kr.co.vocat.vocat.data;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kr.co.vocat.toolbox.GsonRequest;
import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.authenticator.VocatUserManager;
import kr.co.vocat.vocat.data.orm.BookORM;
import kr.co.vocat.vocat.data.orm.SectionORM;
import kr.co.vocat.vocat.data.orm.VocabularyORM;
import kr.co.vocat.vocat.models.Book;
import kr.co.vocat.vocat.models.Section;
import kr.co.vocat.vocat.models.User;
import kr.co.vocat.vocat.models.UserProfile;
import kr.co.vocat.vocat.models.Vocabulary;

/**
 * Created by Shark on 2016-02-03.
 *
 */
public class VocatDBManager {

    private String TAG = "VocatDBManager";

    private Context mContext;
    private VocatUserManager mUserManager;
    private User mUser;
    private UserProfile mUserProfile;
    private HashMap<String, String> mHeaders = new HashMap<>();

    private static String PROFILE_URL = Utils.getAPIURL("accounts/profile/");
    private static String SYNC_URL = Utils.getAPIURL("sync/");
    private static String BOOKS_URL = Utils.getAPIURL("books/");
    private static String SECTIONS_URL = Utils.getAPIURL("sections/");
    private static String VOCABULARIES_URL = Utils.getAPIURL("vocabularies/");
    private static String BOOK_MARKET_URL = Utils.getAPIURL("book-market/");

    public VocatDBManager(Context context) {
        mContext = context;
        mUserManager = new VocatUserManager(context);
        mUser = mUserManager.getUser();
        mUserProfile = mUserManager.getUserProfile();
        String token = mUserManager.getToken();
        mHeaders.put("Authorization", "Token " + token);
    }

    public interface VolleyCallback {
        void onResult(boolean success);
    }
    public interface VolleyBookCallback {
        void onBookResult(Book book);
    }

    public void firstTimeAction(final VolleyCallback callback) {
        Log.d(TAG, "Downloading user profile...");
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET, PROFILE_URL, null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    mUserProfile = new UserProfile(response);
                    try {
                        mUser = new User(response.getJSONObject("user"));
                        Log.d(TAG, "User profile insertion success.");
                        downloadUserBooks(callback);
                    } catch (JSONException e) {
                        Log.d(TAG, "User JSONException due to: " + e.toString());
                        callback.onResult(false);
                    }
                }
            }, new Response.ErrorListener() { @Override public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "User profile download fail due to : " + error.getMessage());
                callback.onResult(false);
            }}) { @Override public Map<String, String> getHeaders() throws AuthFailureError { return mHeaders; }
        };
        Volley.newRequestQueue(mContext).add(myReq);
    }

    private void downloadUserBooks(final VolleyCallback callback) {
        Log.d(TAG, "Downloading user books...");
        GsonRequest<Book[]> myReq = new GsonRequest<>(SYNC_URL, Book[].class, mHeaders,
            new Response.Listener<Book[]>() {
                @Override
                public void onResponse(final Book[] books) {
                    final Handler handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            Bundle bundle = msg.getData();
                            Boolean error = bundle.getBoolean("error");
                            if (error) { callback.onResult(false); }
                            else {
                                mUserManager.setUser(mUser);
                                mUserManager.setUserProfile(mUserProfile);
                                mUserManager.setIsFirstTime(false);
                                mUserManager.setAllCount(0, 0, 0);
                                callback.onResult(true);
                            }
                        }
                    };
                    Runnable r = new Runnable() {
                        public void run() {
                            Message msg = handler.obtainMessage();
                            Bundle bundle = new Bundle();
                            boolean error = false;
                            for (Book book : books) {
                                if(!BookORM.insertBook(mContext, book)) { error = true; break; }
                            }
                            bundle.putBoolean("error", error);
                            msg.setData(bundle);
                            handler.sendMessage(msg);
                        }
                    };
                    Thread myThread = new Thread(r);
                    myThread.start();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "User books download fail due to : " + error.getMessage());
                    callback.onResult(false);
                }
            }
        );
        Volley.newRequestQueue(mContext).add(myReq);
    }

    public void downloadBook(String bookId, final VolleyCallback callback) {
        Log.d(TAG, "Downloading book...");
        GsonRequest<Book> myReq = new GsonRequest<>(BOOK_MARKET_URL + bookId + "/", Book.class, mHeaders,
            new Response.Listener<Book>() {
                @Override
                public void onResponse(Book book) {
                    // TODO: Separate Thread
                    callback.onResult(BookORM.insertBook(mContext, book));
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Books download fail due to : " + error.getMessage());
                    callback.onResult(false);
                }
            }
        );
        Volley.newRequestQueue(mContext).add(myReq);
    }

    public void getBookOnline(String bookId, final VolleyBookCallback callback) {
        Log.d(TAG, "Downloading book...");
        GsonRequest<Book> myReq = new GsonRequest<>(BOOKS_URL + bookId + "/", Book.class, mHeaders,
                new Response.Listener<Book>() {
                    @Override
                    public void onResponse(Book book) {
                        callback.onBookResult(book);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Books download fail due to : " + error.getMessage());
                callback.onBookResult(null);
            }
        }
        );
        Volley.newRequestQueue(mContext).add(myReq);
    }


    public Book generateBook() {
        Log.d(TAG, "Generating book object...");
        Integer bookCount = mUserManager.getBookCount();
        String book_id = String.valueOf(mUserManager.getDeviceId()) + "-" + String.valueOf(bookCount+1);
        Integer user_id = mUser.getId();
        String date_now = Utils.getCurrentTimeStamp();
        String language_for = mUserProfile.getLanguageFor();
        String language_by = mUserProfile.getLanguageBy();

        return new Book(book_id, date_now, date_now, language_for, language_by,
                null, user_id, user_id, "", "", false);
    }

    public Section generateSection(String book, Integer type, Integer sort, String title, String description) {
        Log.d(TAG, "Generating section object...");
        String date_now = Utils.getCurrentTimeStamp();
        Integer sectionCount = mUserManager.getSectionCount();
        String section_id = String.valueOf(mUserManager.getDeviceId()) + "-" + String.valueOf(sectionCount+1);
        return new Section(section_id, date_now, book, type, sort, title, description);
    }

    public boolean createBook(Book book) {
        if (BookORM.insertBook(mContext, book)) {
            Section section = generateSection(book.getId(), 0, 0, "default", "");
            if (createSection(section)) {
                Integer bookCount = mUserManager.getBookCount();
                Integer sectionCount = mUserManager.getSectionCount();
                mUserManager.setBookCount(bookCount + 1);
                mUserManager.setSectionCount(sectionCount + 1);

                if (Utils.isNetworkAvailable(mContext)) {
                    pushBookSection(book, section);
                }
                return true;
            } else {
                BookORM.deleteBookById(mContext, book.getId());
                return false;
            }
        } else { return false; }
    }

    private boolean pushBookSection(Book book, final Section section) {
        Log.d(TAG, "Pushing book object to server...");
        JSONObject jBook;
        try { jBook = new JSONObject(new Gson().toJson(book)); }
        catch (JSONException e) {
            Log.d(TAG, "JSONException error occurred");
            return false;
        }
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST, BOOKS_URL, jBook,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Book created on server");
                    pushSection(section);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Book creation response error due to: " + error.toString());
                }
            }
        ) { @Override public Map<String, String> getHeaders() throws AuthFailureError { return mHeaders; }};
        Volley.newRequestQueue(mContext).add(myReq);
        return true;
    }

    public boolean updateBook(Book book) {
        if (BookORM.updateBook(mContext, book)) {
            Log.d(TAG, "Local book update success.");
            if (Utils.isNetworkAvailable(mContext)) {
                updateServerBook(book);
            }
            return true;
        } else {
            Log.d(TAG, "Local book update fail.");
            return false;
        }
    }

    public boolean updateServerBook(Book book) {
        String url = BOOKS_URL + book.getId() + "/";
        Log.d(TAG, "Updating book object to server...");
        JSONObject jBook;
        try { jBook = new JSONObject(new Gson().toJson(book)); }
        catch (JSONException e) {
            Log.d(TAG, "JSONException error occurred");
            return false;
        }

        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.PUT, url, jBook,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Book updated on server");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Book update response error due to: " + error.toString());
            }
        }
        ) { @Override public Map<String, String> getHeaders() throws AuthFailureError { return mHeaders; }};
        Volley.newRequestQueue(mContext).add(myReq);
        return true;
    }

    public boolean deleteBook(String bookId) {
        Book book = BookORM.findBookById(mContext, bookId);
        if (BookORM.deleteBookById(mContext, bookId)) {
            Log.d(TAG, "Local book deletion success.");
            if (!book.getIsPublic() && Utils.isNetworkAvailable(mContext)) {
                deleteServerBook(bookId);
            }
            return true;
        } else {
            Log.d(TAG, "Local book deletion fail.");
            return false;
        }
    }

    public boolean deleteServerBook(String bookId) {
        String url = BOOKS_URL + bookId + "/";
        StringRequest myReq = new StringRequest(Request.Method.DELETE, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Book deleted on server: " + response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Book deletion response error due to: " + error.toString());
                }
            }
        ) { @Override public Map<String, String> getHeaders() throws AuthFailureError { return mHeaders; }};
        Volley.newRequestQueue(mContext).add(myReq);
        return true;
    }

    public boolean createSection(Section section) {
        if (SectionORM.insertSection(mContext, section)) {
            if (section.getType() != 0 && Utils.isNetworkAvailable(mContext)) {
                pushSection(section);
            }
            return true;
        } else { return false; }
    }

    private boolean pushSection(Section section) {
        Log.d(TAG, "Pushing section object to server...");
        JSONObject jSection;
        try { jSection = new JSONObject(new Gson().toJson(section)); }
        catch (JSONException e) {
            Log.d(TAG, "JSONException error occurred");
            return false;
        }

        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.POST, SECTIONS_URL, jSection,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Section created on server");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Section creation response error due to: " + error.toString());
                }
            }
        ) { @Override public Map<String, String> getHeaders() throws AuthFailureError { return mHeaders; }};
        Volley.newRequestQueue(mContext).add(myReq);
        return true;
    }


    public void updateSection(Section section) {

    }

    public boolean deleteSection(String sectionId) {
        if (SectionORM.deleteSectionById(mContext, sectionId)) {
            Log.d(TAG, "Local section deletion success.");
            if (Utils.isNetworkAvailable(mContext)) {
                deleteServerSection(sectionId);
            }
            return true;
        } else {
            Log.d(TAG, "Local section deletion fail.");
            return false;
        }
    }

    public boolean deleteServerSection(String sectionId) {
        String url = SECTIONS_URL + sectionId + "/";
        StringRequest myReq = new StringRequest(Request.Method.DELETE, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Section deleted on server: " + response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Section deletion response error due to: " + error.toString());
                }
            }
        ) { @Override public Map<String, String> getHeaders() throws AuthFailureError { return mHeaders; }};
        Volley.newRequestQueue(mContext).add(myReq);
        return true;
    }

    public void createVocabulary(Vocabulary vocabulary) {

    }

    public void updateVocabulary(Vocabulary vocabulary) {

    }

    public void deleteVocabulary(String vocabularyId) {

    }
}
