package kr.co.vocat.vocat.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.authenticator.VocatUserManager;

public class IntroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        // ActionBar actionBar = getActionBar();
        // actionBar.hide();
        setContentView(R.layout.activity_intro);

        LinearLayout layoutForFirst = (LinearLayout) findViewById(R.id.layout_for_first);
        VocatUserManager user = new VocatUserManager(this);
        //user.logout();
        if (user.isLoggedIn()) {
            layoutForFirst.setVisibility(View.GONE);
            callNextActivity();
        }

        Button btnSignIn = (Button) findViewById(R.id.btn_sign_in);
        Button btnSignUp = (Button) findViewById(R.id.btn_sign_up);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vocatStartActivity(new Intent(IntroActivity.this, LoginActivity.class));
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vocatStartActivity(new Intent(IntroActivity.this, RegistrationActivity.class));
            }
        });
    }

    private void vocatStartActivity(Intent intent) {
        startActivity(intent);
        //finish();
    }

    private void callNextActivity() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(IntroActivity.this, MainActivity.class);
                startActivity(intent);
                finish(); // never show again
            }
        }, 900);
    }
}
