package kr.co.vocat.vocat.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.vocat.vocat.R;
import kr.co.vocat.vocat.Utils;
import kr.co.vocat.vocat.authenticator.VocatUserManager;
import kr.co.vocat.vocat.data.VocatDBManager;
import kr.co.vocat.vocat.fragments.BookMarketFragment;
import kr.co.vocat.vocat.fragments.BooksFragment;
import kr.co.vocat.vocat.fragments.DashboardFragment;
import kr.co.vocat.vocat.fragments.KnowhowFeedFragment;
import kr.co.vocat.vocat.fragments.StudyFragment;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";

    boolean isFirstMenu = true;

    private View mProgressView;
    private CoordinatorLayout mCoordinatorLayout;
    private LinearLayout mMainLayout;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My title");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        // Shark's code
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        mProgressView = findViewById(R.id.first_time_progress);
        mMainLayout = (LinearLayout) findViewById(R.id.main_layout);
        initiateApp();
    }
    private void initiateApp() {
        VocatUserManager userManager = new VocatUserManager(this);
        if (userManager.isFirstTime()) {
            // 1. Clear all data except Token and deviceId
            userManager.clearAllPrefExceptTD();
            // Get UserProfile and save it to Preferences
            if (!Utils.isNetworkAvailable(this)) {
                Snackbar.make(mCoordinatorLayout,
                        R.string.snkbar_no_internet, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.snkbar_actn_retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                initiateApp();
                            }
                        }).show();
            } else {
                showProgress(true);
                VocatDBManager dbManager = new VocatDBManager(this);
                dbManager.firstTimeAction(new VocatDBManager.VolleyCallback() {
                    @Override
                    public void onResult(boolean success) {
                        showProgress(false);
                        if (success) { initiateApp(); }
                        else {
                            Snackbar.make(mCoordinatorLayout, "Failed to initiate due to some reason.", Snackbar.LENGTH_INDEFINITE);
                        }
                    }
                });
            }
        } else {
            // Show snackbar with Name
            Snackbar.make(mCoordinatorLayout,
                    (getString(R.string.snkbar_welcome) + " " + userManager.getUser().getName() + "!"),
                    Snackbar.LENGTH_SHORT).show();
            // Set drawer's name and emails
            //View header = mNavigationView.inflateHeaderView(R.layout.nav_header_main);
            View header = mNavigationView.getHeaderView(0);
            TextView headerName = (TextView) header.findViewById(R.id.nav_header_name);
            TextView headerEmail = (TextView) header.findViewById(R.id.nav_header_email);
            headerName.setText(userManager.getUser().getName());
            headerEmail.setText(userManager.getUser().getEmail());
            // Select first menu of the drawer.
            selectFirstMenu();

        }
    }

    private void selectFirstMenu() {
        mNavigationView.setCheckedItem(R.id.nav_dashboard);
        mNavigationView.getMenu().performIdentifierAction(R.id.nav_dashboard, 0);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mMainLayout.setVisibility(show ? View.GONE : View.VISIBLE);
            mMainLayout.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMainLayout.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mMainLayout.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (isFirstMenu) super.onBackPressed();
            else selectFirstMenu();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            new VocatUserManager(this).logout();
            Intent mStartActivity = new Intent(this, IntroActivity.class);
            int mPendingIntentId = 123456;
            PendingIntent mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
            System.exit(0);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openFragment(final Fragment fragment, String TAG) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment, TAG)
                .commit();
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        isFirstMenu = false;
        switch (item.getItemId()) {
            case R.id.nav_dashboard:
                isFirstMenu = true;
                openFragment(new DashboardFragment(), "DASHBOARD");
                break;
            case R.id.nav_knowhow_feed:
                openFragment(new KnowhowFeedFragment(), "KNOWHOW_FEED");
                break;
            case R.id.nav_knowhow:
                Log.d(TAG, "Knowhow");
                //openFragment(new KnowhowFragment(), "KNOWHOW");
                break;
            case R.id.nav_study:
                openFragment(new StudyFragment(), "STUDY");
                break;
            case R.id.nav_books:
                openFragment(new BooksFragment(), "BOOKS");
                break;
            case R.id.nav_book_market:
                openFragment(new BookMarketFragment(), "BOOK_MARKET");
                break;
            case R.id.nav_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_feedback:
                intent = new Intent(this, SendFeedbackActivity.class);
                startActivity(intent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
